/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server.ethernet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpResults;
import android.net.EthernetManager;
import android.net.IEthernetServiceListener;
import android.net.InterfaceConfiguration;
import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.net.LinkProperties;
import android.net.LinkAddress;
import android.net.NetworkAgent;
import android.net.NetworkCapabilities;
import android.net.NetworkFactory;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.NetworkUtils;
import android.net.StaticIpConfiguration;
import android.net.RouteInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.ConditionVariable;
import android.os.Looper;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.Log;
import android.content.Intent;
import android.os.UserHandle;
import android.provider.Settings;
import android.os.Message;
import android.os.HandlerThread;
import android.os.SystemProperties;
import android.os.INetworkManagementService;
import android.net.ip.IIpClient;
import android.net.ip.IpClientUtil;
import android.net.shared.ProvisioningConfiguration;
import android.net.ip.IpClientCallbacks;

import static android.net.shared.LinkPropertiesParcelableUtil.toStableParcelable;

import com.android.internal.util.IndentingPrintWriter;
import com.android.server.net.BaseNetworkObserver;

import java.io.FileDescriptor;
import java.io.PrintWriter;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Exception;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.net.InetAddress;
import java.net.Inet4Address;

class EthernetNetworkFactoryExt {
    private static final String TAG = "EthernetNetworkFactoryExt";
    private static String mIface = "eth1";
    private static boolean mLinkUp = false;
    private static String mMode = "0";
    private static String mCurrentMode;
    private static final String DHCP_MODE = "0";
    private static final String STATIC_MODE = "1";
    private static final int DISCONNECTED = 0;
    private static final int CONNECTED = 1;
    private static final int CONNECTING = 2;
    private static final int EVENT_INTERFACE_LINK_STATE_CHANGED = 0;
    private static final int EVENT_INTERFACE_LINK_STATE_CHANGED_DELAY_MS = 1000;
    private static final boolean DBG = true;

    private INetworkManagementService mNMService;
    private Context mContext;
    private Handler mHandler;
    private int mConnectState;
    private LinkProperties mLinkProperties;
    private IIpClient mIpClient;
    private IpClientCallbacksImpl mIpClientCallback;
    private EthernetManager mEthernetManager;
    private StaticIpConfiguration mStaticIpConfiguration;

    public EthernetNetworkFactoryExt() {
        HandlerThread handlerThread = new HandlerThread("EthernetNetworkFactoryExtThread");
        handlerThread.start();
        mHandler = new EthernetNetworkFactoryExtHandler(handlerThread.getLooper(), this);
        mConnectState = DISCONNECTED;
        mCurrentMode = SystemProperties.get("persist.net.ethx.mode", "0");
        mIpClient = null;
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                try {
                    mMode = SystemProperties.get("persist.net.ethx.mode", "0");
                    if (!mMode.equals(mCurrentMode)) {
                        Log.i(TAG, "Mode has been switched,disconnect-connect!");
                        disconnect();
                        connect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timer.schedule(task, 0, 2500);
    }

    private class EthernetNetworkFactoryExtHandler extends Handler {
        private EthernetNetworkFactoryExt mEthernetNetworkFactoryExt;

        public EthernetNetworkFactoryExtHandler(Looper looper, EthernetNetworkFactoryExt factory) {
            super(looper);
            mEthernetNetworkFactoryExt = factory;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_INTERFACE_LINK_STATE_CHANGED:
                    if (msg.arg1 == 1) {
                        mEthernetNetworkFactoryExt.connect();
                    } else {
                        mEthernetNetworkFactoryExt.disconnect();
                    }
                break;
            }
        }
    }

    private void setInterfaceUp(String iface) {
        try {
            mNMService.setInterfaceUp(iface);
        } catch (Exception e) {
            Log.e(TAG, "Error setup interface " + iface + ": " + e);
        }
    }

    private void addToLocalNetwork(String iface, List<RouteInfo> routes) {
        try {
            mNMService.addInterfaceToLocalNetwork(iface, routes);
        } catch (RemoteException e) {
            Log.e(TAG, "Failed to add iface to local network " + e);
        }
    }

    public void start(Context context, INetworkManagementService s) {
        mContext = context;
        mNMService = s;
        mEthernetManager = (EthernetManager) context.getSystemService(Context.ETHERNET_SERVICE);
        try {
            final String[] ifaces = mNMService.listInterfaces();
            for (String iface : ifaces) {
                synchronized(this) {
                    if (mIface.equals(iface)) {
                        setInterfaceUp(iface);
                        break;
                    }
                }
            }
        } catch (RemoteException e) {
            Log.e(TAG, "Could not get list of interfaces " + e);
        }
    }

    public void interfaceLinkStateChanged(String iface, boolean up) {
        Log.d(TAG, "interfaceLinkStateChanged: iface = " + iface + ", up = " + up);
        if (!mIface.equals(iface))
            return;
        if (mLinkUp == up)
            return;
        mLinkUp = up;
        if (up) {
            mHandler.removeMessages(EVENT_INTERFACE_LINK_STATE_CHANGED);
            mHandler.sendMessageDelayed(mHandler.obtainMessage(EVENT_INTERFACE_LINK_STATE_CHANGED, 1, 0),
                        EVENT_INTERFACE_LINK_STATE_CHANGED_DELAY_MS);
        } else {
            mHandler.removeMessages(EVENT_INTERFACE_LINK_STATE_CHANGED);
            mHandler.sendMessageDelayed(mHandler.obtainMessage(EVENT_INTERFACE_LINK_STATE_CHANGED, 0, 0),
                    0);
        }
    }

    private class IpClientCallbacksImpl extends IpClientCallbacks {
        private final ConditionVariable mIpClientStartCv = new ConditionVariable(false);
        private final ConditionVariable mIpClientShutdownCv = new ConditionVariable(false);

        @Override
        public void onIpClientCreated(IIpClient ipClient) {
            mIpClient = ipClient;
            mIpClientStartCv.open();
        }

        private void awaitIpClientStart() {
            mIpClientStartCv.block();
        }

        private void awaitIpClientShutdown() {
            mIpClientShutdownCv.block();
        }

        @Override
        public void onProvisioningSuccess(LinkProperties newLp) {
            Log.d(TAG, "onProvisioningSuccess: lp = " + newLp);
            mLinkProperties = newLp;
            if (mLinkProperties != null) {
                List<RouteInfo> routes = mLinkProperties.getRoutes();
                addToLocalNetwork(mIface, routes);
            }
        }

        @Override
        public void onProvisioningFailure(LinkProperties newLp) {
            Log.d(TAG, "onProvisioningFailure: lp = " + newLp);
        }

        @Override
        public void onLinkPropertiesChange(LinkProperties newLp) {
            Log.d(TAG, "onLinkPropertiesChange: lp = " + newLp);
        }

        @Override
        public void onQuit() {
            mIpClient = null;
            mIpClientShutdownCv.open();
        }

    }

    private boolean startDhcp(String iface) {
        mIpClientCallback = new IpClientCallbacksImpl();
        IpClientUtil.makeIpClient(mContext, iface, mIpClientCallback);
        final ProvisioningConfiguration config =
            new ProvisioningConfiguration.Builder()
                .withoutIPv6()
                .withoutIpReachabilityMonitor()
                .withProvisioningTimeoutMs(36 * 1000)
                .build();
        try {
            if (mIpClient != null) {
                mIpClient.startProvisioning(config.toStableParcelable());
            }
        } catch (RemoteException e) {
            e.rethrowFromSystemServer();
        }
        return true;
    }

    private void stopDhcp(String iface) {
        if (mIpClient != null) {
            try {
                mIpClient.shutdown();
            } catch (RemoteException e) {
                Log.e(TAG, "Error stopping IpClient", e);
            }
            mIpClient = null;
        }
    }

    private void setStaticIpConfiguration(){
        mStaticIpConfiguration =new StaticIpConfiguration();
        String mIpAddress = "192.168.1.100";
        int mNetmask = 24;
        String mGateway = "192.168.1.1";
        String mDns1 = "192.168.1.1";
        String mDns2 = "8.8.8.8";

        String mProStaticInfo = SystemProperties.get("persist.net.ethx.staticinfo", null);
        if (!TextUtils.isEmpty(mProStaticInfo)) {
            String mStaticInfo[] = mProStaticInfo.split(",");
            mIpAddress = mStaticInfo[0];
            mNetmask = Integer.parseInt(mStaticInfo[1]);
            if (mStaticInfo.length == 3) {
                if (!TextUtils.isEmpty(mStaticInfo[2])) {
                    mGateway = mStaticInfo[2];
                }
            } else if (mStaticInfo.length == 4) {
                if (!TextUtils.isEmpty(mStaticInfo[2])) {
                    mGateway = mStaticInfo[2];
                }
                if (!TextUtils.isEmpty(mStaticInfo[3])) {
                    mDns1 = mStaticInfo[3];
                }
            } else if (mStaticInfo.length == 5) {
                if (!TextUtils.isEmpty(mStaticInfo[2])) {
                    mGateway = mStaticInfo[2];
                }
                if (!TextUtils.isEmpty(mStaticInfo[3])) {
                    mDns1 = mStaticInfo[3];
                }
                if (!TextUtils.isEmpty(mStaticInfo[4])) {
                    mDns2 = mStaticInfo[4];
                }
            }
        }

        Inet4Address inetAddr = getIPv4Address(mIpAddress);
        int prefixLength = mNetmask;
        InetAddress gatewayAddr =getIPv4Address(mGateway);
        InetAddress dnsAddr = getIPv4Address(mDns1);
        mStaticIpConfiguration.ipAddress = new LinkAddress(inetAddr, prefixLength);

        // eth1 used in LAN, not need gateway dns
        /*
        mStaticIpConfiguration.gateway=gatewayAddr;
        mStaticIpConfiguration.dnsServers.add(dnsAddr);
        mStaticIpConfiguration.dnsServers.add(getIPv4Address(mDns2));
        */
    }

    private boolean setStaticIpAddress(StaticIpConfiguration staticConfig, String iface) {
        if (DBG) Log.d(TAG, "setStaticIpAddress:" + staticConfig);
        mIpClientCallback = new IpClientCallbacksImpl();
        IpClientUtil.makeIpClient(mContext, iface, mIpClientCallback);
        if (staticConfig.ipAddress != null ) {
            final ProvisioningConfiguration config =
                new ProvisioningConfiguration.Builder()
                    .withoutIPv6()
                    .withoutIpReachabilityMonitor()
                    .withProvisioningTimeoutMs(36 * 1000)
                    .withStaticConfiguration(staticConfig)
                    .build();
            Log.i(TAG, "Applying static IPv4 configuration to " + mIface + ": " + staticConfig);
            try {
                if (mIpClient != null) {
                    mIpClient.startProvisioning(config.toStableParcelable());
                }
            } catch(RemoteException e) {
                e.rethrowFromSystemServer();
                return false;
            }
            return true;
        } else {
            Log.e(TAG, "Invalid static IP configuration.");
        }
        return false;
    }

    private void startDhcpServer() {
        if (DBG) Log.d(TAG, "startDhcpServer");
        String startIp = SystemProperties.get("persist.dhcpserver.start", "192.168.1.150");
        String endIp = SystemProperties.get("persist.dhcpserver.end", "192.168.1.250");
        String[] dhcpRange = {startIp, endIp};
        try {
            mNMService.tetherInterface(mIface);
            mNMService.startTethering(dhcpRange);
        } catch (Exception e) {
            Log.e(TAG, "Error tether interface " + mIface + ": " + e);
        }
    }

    private void stopDhcpServer() {
        if (DBG) Log.d(TAG, "stopDhcpServer");
            try {
                mNMService.stopTethering();
            } catch (Exception e) {
                Log.e(TAG, "Error tether stop interface " + mIface + ": " + e);
            }
        }

    private void connect() {
        Thread connectThread = new Thread(new Runnable() {
            public void run() {
            if (mConnectState == CONNECTED) {
                Log.d(TAG, "already connected, skip");
                return;
            }
            mConnectState = CONNECTING;
            mMode = SystemProperties.get("persist.net.ethx.mode", "0");
            mCurrentMode = mMode;
            if (mMode.equals(DHCP_MODE)) { // DHCP
                if (!startDhcp(mIface)) {
                    Log.e(TAG, "startDhcp failed for " + mIface);
                    mConnectState = DISCONNECTED;
                    return;
                }
            } else { // Static
                setStaticIpConfiguration();
                if (!setStaticIpAddress(mStaticIpConfiguration, mIface)) {
                    // We've already logged an error.
                    if (DBG) Log.i(TAG, "setStaticIpAddress error,set again");
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (!setStaticIpAddress(mStaticIpConfiguration, mIface)) {
                        mConnectState = DISCONNECTED;
                        return;
                    }
                }
                mLinkProperties = mStaticIpConfiguration.toLinkProperties(mIface);
                //add dhcpserver
                if (SystemProperties.get("persist.dhcpserver.enable", "0").equals("1")) {
                    startDhcpServer();
                }
            }
            mConnectState = CONNECTED;
            }
        });
        connectThread.start();
    }

    private void disconnect() {
        Thread disconnectThread = new Thread(new Runnable() {
            public void run() {
                if (mConnectState == DISCONNECTED) {
                    Log.d(TAG, "already disconnected, skip");
                    return;
                }
                if (mCurrentMode.equals(DHCP_MODE)) { // DHCP
                    stopDhcp(mIface);
                } else {
                    if (SystemProperties.get("persist.dhcpserver.enable", "0").equals("1")) {
                        stopDhcpServer();
                    }
                }
                try {
                    mNMService.clearInterfaceAddresses(mIface);
                } catch (Exception e) {
                    Log.e(TAG, "Failed to clear addresses " + e);
                }
                mConnectState = DISCONNECTED;
            }
        });
        disconnectThread.start();
    }

    public void interfaceAdded(String iface) {
        Log.d(TAG, "interfaceAdded: iface = " + iface);
        if (!mIface.equals(iface))
            return;
        setInterfaceUp(mIface);
        mLinkUp = false;
    }

    public void interfaceRemoved(String iface) {
        Log.d(TAG, "interfaceRemoved: iface = " + iface);
        if (!mIface.equals(iface))
            return;
        mLinkUp = false;
        mHandler.removeMessages(EVENT_INTERFACE_LINK_STATE_CHANGED);
        disconnect();
    }

    private Inet4Address getIPv4Address(String text) {
        try {
            return (Inet4Address) NetworkUtils.numericToInetAddress(text);
        } catch (IllegalArgumentException|ClassCastException e) {
            return null;
        }
    }

    public String getGateway() {
        for (RouteInfo route : mLinkProperties.getRoutes()) {
            if (route.hasGateway()) {
                InetAddress gateway = route.getGateway();
                if (route.isIPv4Default()) {
                    return gateway.getHostAddress();
                }
            }
        }
        return "";
    }

}
