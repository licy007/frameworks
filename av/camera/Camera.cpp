/*
**
** Copyright (C) 2008, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

//#define LOG_NDEBUG 0
#define LOG_TAG "Camera"
#include <utils/Log.h>
#include <utils/threads.h>
#include <utils/String16.h>
#include <binder/IPCThreadState.h>
#include <binder/IServiceManager.h>
#include <binder/IMemory.h>

#include <Camera.h>
#include <ICameraRecordingProxyListener.h>
#include <android/hardware/ICameraService.h>
#include <android/hardware/ICamera.h>

#include <gui/IGraphicBufferProducer.h>
#include <gui/Surface.h>

namespace android {

Camera::Camera(int cameraId)
    : CameraBase(cameraId)
{
}

CameraTraits<Camera>::TCamConnectService CameraTraits<Camera>::fnConnectService =
        &::android::hardware::ICameraService::connect;

CameraTraits<Camera>::TCamConnectService_aw CameraTraits<Camera>::fnConnectService_aw =
        &::android::hardware::ICameraService::connect_aw;

sp<Camera> Camera::connect_aw(int startId, int cameraNum, int width, int height, int is360View,
        const String16& clientPackageName, int clientUid, int clientPid)
{
    return CameraBaseT::connect_aw(startId, cameraNum, width, height, is360View, clientPackageName, clientUid, clientPid);
}

void Camera::startRender()
{
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return ;
    return c->startRender();
}

void Camera::stopRender()
{
     sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return ;
    return c->stopRender();
}


status_t Camera::awCamRecInit(const String8& filename, int format,
                                  int width, int height, int framerate, int bitrate,
                                  int audiomode, int audiobitrate)
{
    ALOGV("Camera awCamRecInit filename:%s, f:%d, w:%d, h:%d, fr:%d, br:%d, am:%d, abr:%d",
        filename.string(), format, width, height, framerate, bitrate, audiomode, audiobitrate);
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->awCamRecInit(filename, format, width, height, framerate, bitrate, audiomode, audiobitrate);
    ALOGV("Camera awCamRecInit ret = %d",ret);
    return ret;
}

status_t Camera::awCamRecStart()
{
    ALOGV("Camera awCamRecStart!");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->awCamRecStart();
    ALOGV("Camera awCamRecStart ret = %d",ret);
    return ret;
}

status_t Camera::awCamRecStop()
{
    ALOGV("Camera awCamRecStop!");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->awCamRecStop();
    ALOGV("Camera awCamRecStop ret = %d",ret);
    return ret;
}

status_t Camera::awCamRecSetParaAndCmds(const String8& parameters, int cmds)
{
    ALOGV("Camera awCamRecSetParaAndCmds parameters = %s, cmds = %d",parameters.string(),cmds);
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->awCamRecSetParaAndCmds(parameters,cmds);
    ALOGV("Camera awCamRecSetParaAndCmds ret = %d",ret);
    return ret;
}

String8 Camera::awCamRecGetParaAndCmds(int cmds) const
{
    ALOGV("Camera awCamRecGetParaAndCmds cmds = %d",cmds);
    String8 params;
    sp <::android::hardware::ICamera> c = mCamera;
    if (c != 0) {
        params = c->awCamRecGetParaAndCmds(cmds);
        ALOGV("Camera awCamRecGetParaAndCmds parameters = %s, cmds = %d",params.string(),cmds);
    }
    return params;
}

status_t Camera::awCamRecStartNextFile(const String8& filename)
{
    ALOGV("Camera awCamRecStartNextFile parameters = %s",filename.string());
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->awCamRecStartNextFile(filename);
    ALOGV("Camera awCamRecStartNextFile ret = %d",ret);
    return ret;
}

status_t Camera::awCamRecRelease()
{
    ALOGV("Camera awCamRecRelease!");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->awCamRecRelease();
    ALOGV("Camera awCamRecRelease ret = %d",ret);
    return ret;
}

status_t Camera::startWaterMark()
{
    ALOGV("Camera startWaterMark");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->startWaterMark();
    ALOGV("Camera startWaterMark ret = %d",ret);
    return ret;
}

status_t Camera::stopWaterMark()
{
    ALOGV("Camera stopWaterMark");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->stopWaterMark();
    ALOGV("Camera stopWaterMark ret = %d",ret);
    return ret;
}

status_t Camera::setWaterMarkMultiple(const String8& mWaterMark, int dispMode)
{
    ALOGV("Camera setWaterMarkMultiple mWaterMark = %s, dispMode = %d",mWaterMark.string(),dispMode);
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    status_t ret = c->setWaterMarkMultiple(mWaterMark,dispMode);
    ALOGV("Camera setWaterMarkMultiple ret = %d",ret);
    return ret;
}

int Camera::bv360SetVal(int type, int value)
{
    binder::Status serviceRet;
    int ret = -1;
    const sp<::android::hardware::ICameraService>& cs = CameraBaseT::getCameraService();

    if (cs != nullptr) {
        serviceRet = cs.get()->bv360SetVal(type,value,&ret);
        if(!serviceRet.isOk())
        {
            return -1;
        }
        return ret;
    }
    return -1;
}

int Camera::bv360GetVal(int type)
{
    binder::Status serviceRet;
    int ret = -1;
    const sp<::android::hardware::ICameraService>& cs = CameraBaseT::getCameraService();

    if (cs != nullptr) {
        serviceRet = cs.get()->bv360GetVal(type,&ret);
        if(!serviceRet.isOk())
        {
            return -1;
        }
        return ret;
    }
    return -1;
}

int Camera::bv360SetStr(int type, const String8& params)
{
    binder::Status serviceRet;
    int ret = -1;
    const sp<::android::hardware::ICameraService>& cs = CameraBaseT::getCameraService();

    if (cs != nullptr) {
        String16 paramsString16 = String16(params);
        serviceRet = cs.get()->bv360SetStr(type,paramsString16,&ret);
        if(!serviceRet.isOk())
        {
            return -1;
        }
        return ret;
    }
    return -1;
}

String8 Camera::bv360GetStr(int type)
{
    binder::Status serviceRet;
    const sp<::android::hardware::ICameraService>& cs = CameraBaseT::getCameraService();
    String8 params;
    String16 paramsStr16;

    if (cs != nullptr) {
         serviceRet = cs.get()->bv360GetStr(type,&paramsStr16);
         if(!serviceRet.isOk())
         {
            return params;
         }
         params = String8(paramsStr16);
         return params;
    }
    return params;
}

status_t Camera::setEGLPreviewTarget(const sp<IGraphicBufferProducer>& bufferProducer)
{
    ALOGV("setEGLPreviewTarget(%p)", bufferProducer.get());
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    ALOGD_IF(bufferProducer == 0, "app passed NULL surface");
    return c->setEGLPreviewTarget(bufferProducer);
}
status_t Camera::startEGLPreview()
{
    ALOGV("startEGLPreview");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->startEGLPreview();
}

status_t Camera::stopEGLPreview()
{
    ALOGV("stopEGLPreview");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->stopEGLPreview();
}
status_t Camera::setCarSize(float length,float width,float height)
{
    ALOGV("setCarSize");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->setCarSize(length,width,height);
}

status_t Camera::setScreenSize(int width,int height)
{
    ALOGV("setScreenSize");
    sp<::android::hardware::ICamera> c = mCamera;
    if(c == 0) return NO_INIT;
    return c->setScreenSize(width,height);
}

status_t Camera::setCurrentPoint(int pointX,int pointY)
{
    ALOGV("setCurrentPoint");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->setCurrentPoint(pointX,pointY);
}

status_t Camera::manualAdjust(int num,int *pPoints)
{
    ALOGV("manualAdjust");
    sp<::android::hardware::ICamera> c = mCamera;
    if(c == 0) return NO_INIT;
    return c->manualAdjust(num,pPoints);
}

status_t Camera::setTouchData(int type,int count,int *jamoveDis,int scaleDis) {
    ALOGV("setTouchData");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
       return c->setTouchData(type,count,jamoveDis,scaleDis);
}

status_t Camera::setMaxWheelAngleInfo(int maxSteeringAngle,int maxWheelAngle)
{
    ALOGV("setMaxWheelAngleInfo");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->setMaxWheelAngleInfo(maxSteeringAngle,maxWheelAngle);
}

status_t Camera::setCameraPosition(Camera_RVCameraPosition *position)
{
    ALOGV("setCameraPosition");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->setCameraPosition(position);
}

status_t Camera::getCameraPosition(Camera_RVCameraPosition *position)
{
    ALOGV("getCameraPosition");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->getCameraPosition(position);
}

status_t Camera::setStructureInfo(Camera_RVCarStructureInfo *info)
{
    ALOGV("setCameraPosition");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->setStructureInfo(info);
}

status_t Camera::getStructureInfo(Camera_RVCarStructureInfo *info)
{
    ALOGV("setCameraPosition");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->getStructureInfo(info);
}

status_t Camera::getRadarInfo(Camera_RVRadarAllInfos *info)
{
    ALOGV("getRadarInfo");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->getRadarInfo(info);
}

status_t Camera::setRadarInfo(Camera_RVRadarAllInfos *info)
{
    ALOGV("setRadarInfo");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->setRadarInfo(info);
}

status_t Camera::setRadarData(float *pData,int count)
{
    ALOGV("setRadarInfo");
    sp<::android::hardware::ICamera> c= mCamera;
    if(c == 0) return NO_INIT;
    return c->setRadarData(pData,count);
}

// construct a camera client from an existing camera remote
sp<Camera> Camera::create(const sp<::android::hardware::ICamera>& camera)
{
     ALOGV("create");
     if (camera == 0) {
         ALOGE("camera remote is a NULL pointer");
         return 0;
     }

    sp<Camera> c = new Camera(-1);
    if (camera->connect(c) == NO_ERROR) {
        c->mStatus = NO_ERROR;
        c->mCamera = camera;
        IInterface::asBinder(camera)->linkToDeath(c);
        return c;
    }
    return 0;
}

Camera::~Camera()
{
    // We don't need to call disconnect() here because if the CameraService
    // thinks we are the owner of the hardware, it will hold a (strong)
    // reference to us, and we can't possibly be here. We also don't want to
    // call disconnect() here if we are in the same process as mediaserver,
    // because we may be invoked by CameraService::Client::connect() and will
    // deadlock if we call any method of ICamera here.
}

sp<Camera> Camera::connect(int cameraId, const String16& clientPackageName,
        int clientUid, int clientPid)
{
    return CameraBaseT::connect(cameraId, clientPackageName, clientUid, clientPid);
}

status_t Camera::connectLegacy(int cameraId, int halVersion,
        const String16& clientPackageName,
        int clientUid,
        sp<Camera>& camera)
{
    ALOGV("%s: connect legacy camera device", __FUNCTION__);
    sp<Camera> c = new Camera(cameraId);
    sp<::android::hardware::ICameraClient> cl = c;
    status_t status = NO_ERROR;
    const sp<::android::hardware::ICameraService>& cs = CameraBaseT::getCameraService();

    binder::Status ret;
    if (cs != nullptr) {
        ret = cs.get()->connectLegacy(cl, cameraId, halVersion, clientPackageName,
                clientUid, /*out*/&(c->mCamera));
    }
    if (ret.isOk() && c->mCamera != nullptr) {
        IInterface::asBinder(c->mCamera)->linkToDeath(c);
        c->mStatus = NO_ERROR;
        camera = c;
    } else {
        switch(ret.serviceSpecificErrorCode()) {
            case hardware::ICameraService::ERROR_DISCONNECTED:
                status = -ENODEV;
                break;
            case hardware::ICameraService::ERROR_CAMERA_IN_USE:
                status = -EBUSY;
                break;
            case hardware::ICameraService::ERROR_INVALID_OPERATION:
                status = -EINVAL;
                break;
            case hardware::ICameraService::ERROR_MAX_CAMERAS_IN_USE:
                status = -EUSERS;
                break;
            case hardware::ICameraService::ERROR_ILLEGAL_ARGUMENT:
                status = BAD_VALUE;
                break;
            case hardware::ICameraService::ERROR_DEPRECATED_HAL:
                status = -EOPNOTSUPP;
                break;
            case hardware::ICameraService::ERROR_DISABLED:
                status = -EACCES;
                break;
            case hardware::ICameraService::ERROR_PERMISSION_DENIED:
                status = PERMISSION_DENIED;
                break;
            default:
                status = -EINVAL;
                ALOGW("An error occurred while connecting to camera %d: %s", cameraId,
                        (cs != nullptr) ? "Service not available" : ret.toString8().string());
                break;
        }
        c.clear();
    }
    return status;
}

status_t Camera::reconnect()
{
    ALOGV("reconnect");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->connect(this);
}

status_t Camera::lock()
{
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->lock();
}

status_t Camera::unlock()
{
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->unlock();
}

// pass the buffered IGraphicBufferProducer to the camera service
status_t Camera::setPreviewTarget(const sp<IGraphicBufferProducer>& bufferProducer)
{
    ALOGV("setPreviewTarget(%p)", bufferProducer.get());
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    ALOGD_IF(bufferProducer == 0, "app passed NULL surface");
    return c->setPreviewTarget(bufferProducer);
}

status_t Camera::setVideoTarget(const sp<IGraphicBufferProducer>& bufferProducer)
{
    ALOGV("setVideoTarget(%p)", bufferProducer.get());
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    ALOGD_IF(bufferProducer == 0, "app passed NULL video surface");
    return c->setVideoTarget(bufferProducer);
}

// start preview mode
status_t Camera::startPreview()
{
    ALOGV("startPreview");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->startPreview();
}

status_t Camera::setVideoBufferMode(int32_t videoBufferMode)
{
    ALOGV("setVideoBufferMode: %d", videoBufferMode);
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->setVideoBufferMode(videoBufferMode);
}

// start recording mode, must call setPreviewTarget first
status_t Camera::startRecording()
{
    ALOGV("startRecording");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->startRecording();
}

// stop preview mode
void Camera::stopPreview()
{
    ALOGV("stopPreview");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return;
    c->stopPreview();
}

// stop recording mode
void Camera::stopRecording()
{
    ALOGV("stopRecording");
    {
        Mutex::Autolock _l(mLock);
        mRecordingProxyListener.clear();
    }
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return;
    c->stopRecording();
}

// release a recording frame
void Camera::releaseRecordingFrame(const sp<IMemory>& mem)
{
    ALOGV("releaseRecordingFrame");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return;
    c->releaseRecordingFrame(mem);
}

void Camera::releaseRecordingFrameHandle(native_handle_t* handle)
{
    ALOGV("releaseRecordingFrameHandle");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return;
    c->releaseRecordingFrameHandle(handle);
}

void Camera::releaseRecordingFrameHandleBatch(
        const std::vector<native_handle_t*> handles) {
    ALOGV("releaseRecordingFrameHandleBatch");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return;
    c->releaseRecordingFrameHandleBatch(handles);
}

// get preview state
bool Camera::previewEnabled()
{
    ALOGV("previewEnabled");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return false;
    return c->previewEnabled();
}

// get recording state
bool Camera::recordingEnabled()
{
    ALOGV("recordingEnabled");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return false;
    return c->recordingEnabled();
}

status_t Camera::autoFocus()
{
    ALOGV("autoFocus");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->autoFocus();
}

status_t Camera::cancelAutoFocus()
{
    ALOGV("cancelAutoFocus");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->cancelAutoFocus();
}

// take a picture
status_t Camera::takePicture(int msgType)
{
    ALOGV("takePicture: 0x%x", msgType);
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->takePicture(msgType);
}

// set preview/capture parameters - key/value pairs
status_t Camera::setParameters(const String8& params)
{
    ALOGV("setParameters");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->setParameters(params);
}

// get preview/capture parameters - key/value pairs
String8 Camera::getParameters() const
{
    ALOGV("getParameters");
    String8 params;
    sp <::android::hardware::ICamera> c = mCamera;
    if (c != 0) params = mCamera->getParameters();
    return params;
}

// send command to camera driver
status_t Camera::sendCommand(int32_t cmd, int32_t arg1, int32_t arg2)
{
    ALOGV("sendCommand");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->sendCommand(cmd, arg1, arg2);
}

void Camera::setListener(const sp<CameraListener>& listener)
{
    Mutex::Autolock _l(mLock);
    mListener = listener;
}

void Camera::setRecordingProxyListener(const sp<ICameraRecordingProxyListener>& listener)
{
    Mutex::Autolock _l(mLock);
    mRecordingProxyListener = listener;
}

void Camera::setPreviewCallbackFlags(int flag)
{
    ALOGV("setPreviewCallbackFlags");
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return;
    mCamera->setPreviewCallbackFlag(flag);
}

status_t Camera::setPreviewCallbackTarget(
        const sp<IGraphicBufferProducer>& callbackProducer)
{
    sp <::android::hardware::ICamera> c = mCamera;
    if (c == 0) return NO_INIT;
    return c->setPreviewCallbackTarget(callbackProducer);
}

// callback from camera service
void Camera::notifyCallback(int32_t msgType, int32_t ext1, int32_t ext2)
{
    return CameraBaseT::notifyCallback(msgType, ext1, ext2);
}

// callback from camera service when frame or image is ready
void Camera::dataCallback(int32_t msgType, const sp<IMemory>& dataPtr,
                          camera_frame_metadata_t *metadata)
{
    sp<CameraListener> listener;
    {
        Mutex::Autolock _l(mLock);
        listener = mListener;
    }
    if (listener != NULL) {
        listener->postData(msgType, dataPtr, metadata);
    }
}

// callback from camera service when timestamped frame is ready
void Camera::dataCallbackTimestamp(nsecs_t timestamp, int32_t msgType, const sp<IMemory>& dataPtr)
{
    // If recording proxy listener is registered, forward the frame and return.
    // The other listener (mListener) is ignored because the receiver needs to
    // call releaseRecordingFrame.
    sp<ICameraRecordingProxyListener> proxylistener;
    {
        Mutex::Autolock _l(mLock);
        proxylistener = mRecordingProxyListener;
    }
    if (proxylistener != NULL) {
        proxylistener->dataCallbackTimestamp(timestamp, msgType, dataPtr);
        return;
    }

    sp<CameraListener> listener;
    {
        Mutex::Autolock _l(mLock);
        listener = mListener;
    }

    if (listener != NULL) {
        listener->postDataTimestamp(timestamp, msgType, dataPtr);
    } else {
        ALOGW("No listener was set. Drop a recording frame.");
        releaseRecordingFrame(dataPtr);
    }
}

void Camera::recordingFrameHandleCallbackTimestamp(nsecs_t timestamp, native_handle_t* handle)
{
    // If recording proxy listener is registered, forward the frame and return.
    // The other listener (mListener) is ignored because the receiver needs to
    // call releaseRecordingFrameHandle.
    sp<ICameraRecordingProxyListener> proxylistener;
    {
        Mutex::Autolock _l(mLock);
        proxylistener = mRecordingProxyListener;
    }
    if (proxylistener != NULL) {
        proxylistener->recordingFrameHandleCallbackTimestamp(timestamp, handle);
        return;
    }

    sp<CameraListener> listener;
    {
        Mutex::Autolock _l(mLock);
        listener = mListener;
    }

    if (listener != NULL) {
        listener->postRecordingFrameHandleTimestamp(timestamp, handle);
    } else {
        ALOGW("No listener was set. Drop a recording frame.");
        releaseRecordingFrameHandle(handle);
    }
}

void Camera::recordingFrameHandleCallbackTimestampBatch(
        const std::vector<nsecs_t>& timestamps,
        const std::vector<native_handle_t*>& handles)
{
    // If recording proxy listener is registered, forward the frame and return.
    // The other listener (mListener) is ignored because the receiver needs to
    // call releaseRecordingFrameHandle.
    sp<ICameraRecordingProxyListener> proxylistener;
    {
        Mutex::Autolock _l(mLock);
        proxylistener = mRecordingProxyListener;
    }
    if (proxylistener != NULL) {
        proxylistener->recordingFrameHandleCallbackTimestampBatch(timestamps, handles);
        return;
    }

    sp<CameraListener> listener;
    {
        Mutex::Autolock _l(mLock);
        listener = mListener;
    }

    if (listener != NULL) {
        listener->postRecordingFrameHandleTimestampBatch(timestamps, handles);
    } else {
        ALOGW("No listener was set. Drop a batch of recording frames.");
        releaseRecordingFrameHandleBatch(handles);
    }
}

sp<ICameraRecordingProxy> Camera::getRecordingProxy() {
    ALOGV("getProxy");
    return new RecordingProxy(this);
}

status_t Camera::RecordingProxy::startRecording(const sp<ICameraRecordingProxyListener>& listener)
{
    ALOGV("RecordingProxy::startRecording");
    mCamera->setRecordingProxyListener(listener);
    mCamera->reconnect();
    return mCamera->startRecording();
}

void Camera::RecordingProxy::stopRecording()
{
    ALOGV("RecordingProxy::stopRecording");
    mCamera->stopRecording();
}

void Camera::RecordingProxy::releaseRecordingFrame(const sp<IMemory>& mem)
{
    ALOGV("RecordingProxy::releaseRecordingFrame");
    mCamera->releaseRecordingFrame(mem);
}

void Camera::RecordingProxy::releaseRecordingFrameHandle(native_handle_t* handle) {
    ALOGV("RecordingProxy::releaseRecordingFrameHandle");
    mCamera->releaseRecordingFrameHandle(handle);
}

void Camera::RecordingProxy::releaseRecordingFrameHandleBatch(
        const std::vector<native_handle_t*>& handles) {
    ALOGV("RecordingProxy::releaseRecordingFrameHandleBatch");
    mCamera->releaseRecordingFrameHandleBatch(handles);
}

Camera::RecordingProxy::RecordingProxy(const sp<Camera>& camera)
{
    mCamera = camera;
}

}; // namespace android
