/*
**
** Copyright 2008, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/

//#define LOG_NDEBUG 0
#define LOG_TAG "ICamera"
#include <utils/Log.h>
#include <stdint.h>
#include <sys/types.h>
#include <binder/Parcel.h>
#include <camera/CameraUtils.h>
#include <android/hardware/ICamera.h>
#include <android/hardware/ICameraClient.h>
#include <gui/IGraphicBufferProducer.h>
#include <gui/Surface.h>
#include <media/hardware/HardwareAPI.h>

namespace android {
namespace hardware {

enum {
    DISCONNECT = IBinder::FIRST_CALL_TRANSACTION,
    SET_PREVIEW_TARGET,
    SET_PREVIEW_CALLBACK_FLAG,
    SET_PREVIEW_CALLBACK_TARGET,
    START_PREVIEW,
    STOP_PREVIEW,
    AUTO_FOCUS,
    CANCEL_AUTO_FOCUS,
    TAKE_PICTURE,
    SET_PARAMETERS,
    GET_PARAMETERS,
    SEND_COMMAND,
    CONNECT,
    LOCK,
    UNLOCK,
    PREVIEW_ENABLED,
    START_RECORDING,
    STOP_RECORDING,
    RECORDING_ENABLED,
    RELEASE_RECORDING_FRAME,
    SET_VIDEO_BUFFER_MODE,
    SET_VIDEO_BUFFER_TARGET,
    RELEASE_RECORDING_FRAME_HANDLE,
    RELEASE_RECORDING_FRAME_HANDLE_BATCH,
    AW_CAM_REC_INIT,
    AW_CAM_REC_START,
    AW_CAM_REC_STOP,
    AW_CAM_REC_SET_PARA_AND_CMDS,
    AW_CAM_REC_GET_PARA_AND_CMDS,
    AW_CAM_REC_START_NEXT_FILE,
    AW_CAM_REC_RELEASE,
    AW_CAM_START_RENDER,
    AW_CAM_STOP_RENDER,
    AW_START_WATER_MARK,
    AW_STOP_WATER_MARK,
    SET_WATER_MARK_MULTIPLE,
    SET_EGL_PREVIEW_TARGET,
    START_EGL_PREVIEW,
    STOP_EGL_PREVIEW,
    SET_CAR_SIZE,
    SET_SCREEN_SIZE,
    SET_CURRENT_POINT,
    MANUAL_ADJUST,
    SET_TOUCH_DATA,
    SET_MAX_WHEEL_INFO,
    SET_CAMERA_POSITION,
    SET_STRUCT_INFO,
    GET_CAMERA_POSITION,
    GET_STRUCT_INFO,
    GET_RADAR_INFO,
    SET_RADAR_INFO,
    SET_RADAR_DATA,
};

class BpCamera: public BpInterface<ICamera>
{
public:
    explicit BpCamera(const sp<IBinder>& impl)
        : BpInterface<ICamera>(impl)
    {
    }

    // disconnect from camera service
    binder::Status disconnect()
    {
        ALOGV("disconnect");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(DISCONNECT, data, &reply);
        reply.readExceptionCode();
        return binder::Status::ok();
    }

    // pass the buffered IGraphicBufferProducer to the camera service
    status_t setPreviewTarget(const sp<IGraphicBufferProducer>& bufferProducer)
    {
        ALOGV("setPreviewTarget");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        sp<IBinder> b(IInterface::asBinder(bufferProducer));
        data.writeStrongBinder(b);
        remote()->transact(SET_PREVIEW_TARGET, data, &reply);
        return reply.readInt32();
    }

    // set the preview callback flag to affect how the received frames from
    // preview are handled. See Camera.h for details.
    void setPreviewCallbackFlag(int flag)
    {
        ALOGV("setPreviewCallbackFlag(%d)", flag);
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(flag);
        remote()->transact(SET_PREVIEW_CALLBACK_FLAG, data, &reply);
    }

    status_t setPreviewCallbackTarget(
            const sp<IGraphicBufferProducer>& callbackProducer)
    {
        ALOGV("setPreviewCallbackTarget");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        sp<IBinder> b(IInterface::asBinder(callbackProducer));
        data.writeStrongBinder(b);
        remote()->transact(SET_PREVIEW_CALLBACK_TARGET, data, &reply);
        return reply.readInt32();
    }

    // start preview mode, must call setPreviewTarget first
    status_t startPreview()
    {
        ALOGV("startPreview");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(START_PREVIEW, data, &reply);
        return reply.readInt32();
    }

    // start recording mode, must call setPreviewTarget first
    status_t startRecording()
    {
        ALOGV("startRecording");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(START_RECORDING, data, &reply);
        return reply.readInt32();
    }

    // stop preview mode
    void stopPreview()
    {
        ALOGV("stopPreview");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(STOP_PREVIEW, data, &reply);
    }

    // stop recording mode
    void stopRecording()
    {
        ALOGV("stopRecording");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(STOP_RECORDING, data, &reply);
    }

    void releaseRecordingFrame(const sp<IMemory>& mem)
    {
        ALOGV("releaseRecordingFrame");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeStrongBinder(IInterface::asBinder(mem));

        remote()->transact(RELEASE_RECORDING_FRAME, data, &reply);
    }

    void releaseRecordingFrameHandle(native_handle_t *handle) {
        ALOGV("releaseRecordingFrameHandle");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeNativeHandle(handle);

        remote()->transact(RELEASE_RECORDING_FRAME_HANDLE, data, &reply);

        // Close the native handle because camera received a dup copy.
        native_handle_close(handle);
        native_handle_delete(handle);
    }

    void releaseRecordingFrameHandleBatch(const std::vector<native_handle_t*>& handles) {
        ALOGV("releaseRecordingFrameHandleBatch");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        uint32_t n = handles.size();
        data.writeUint32(n);
        for (auto& handle : handles) {
            data.writeNativeHandle(handle);
        }
        remote()->transact(RELEASE_RECORDING_FRAME_HANDLE_BATCH, data, &reply);

        // Close the native handle because camera received a dup copy.
        for (auto& handle : handles) {
            native_handle_close(handle);
            native_handle_delete(handle);
        }
    }

    status_t setVideoBufferMode(int32_t videoBufferMode)
    {
        ALOGV("setVideoBufferMode: %d", videoBufferMode);
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(videoBufferMode);
        remote()->transact(SET_VIDEO_BUFFER_MODE, data, &reply);
        return reply.readInt32();
    }

    // check preview state
    bool previewEnabled()
    {
        ALOGV("previewEnabled");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(PREVIEW_ENABLED, data, &reply);
        return reply.readInt32();
    }

    // check recording state
    bool recordingEnabled()
    {
        ALOGV("recordingEnabled");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(RECORDING_ENABLED, data, &reply);
        return reply.readInt32();
    }

    // auto focus
    status_t autoFocus()
    {
        ALOGV("autoFocus");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(AUTO_FOCUS, data, &reply);
        status_t ret = reply.readInt32();
        return ret;
    }

    // cancel focus
    status_t cancelAutoFocus()
    {
        ALOGV("cancelAutoFocus");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(CANCEL_AUTO_FOCUS, data, &reply);
        status_t ret = reply.readInt32();
        return ret;
    }

    // take a picture - returns an IMemory (ref-counted mmap)
    status_t takePicture(int msgType)
    {
        ALOGV("takePicture: 0x%x", msgType);
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(msgType);
        remote()->transact(TAKE_PICTURE, data, &reply);
        status_t ret = reply.readInt32();
        return ret;
    }

    // set preview/capture parameters - key/value pairs
    status_t setParameters(const String8& params)
    {
        ALOGV("setParameters");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeString8(params);
        remote()->transact(SET_PARAMETERS, data, &reply);
        return reply.readInt32();
    }

    // get preview/capture parameters - key/value pairs
    String8 getParameters() const
    {
        ALOGV("getParameters");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(GET_PARAMETERS, data, &reply);
        return reply.readString8();
    }
    virtual status_t sendCommand(int32_t cmd, int32_t arg1, int32_t arg2)
    {
        ALOGV("sendCommand");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(cmd);
        data.writeInt32(arg1);
        data.writeInt32(arg2);
        remote()->transact(SEND_COMMAND, data, &reply);
        return reply.readInt32();
    }
    virtual status_t connect(const sp<ICameraClient>& cameraClient)
    {
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeStrongBinder(IInterface::asBinder(cameraClient));
        remote()->transact(CONNECT, data, &reply);
        return reply.readInt32();
    }
    virtual status_t lock()
    {
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(LOCK, data, &reply);
        return reply.readInt32();
    }
    virtual status_t unlock()
    {
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(UNLOCK, data, &reply);
        return reply.readInt32();
    }

    status_t setVideoTarget(const sp<IGraphicBufferProducer>& bufferProducer)
    {
        ALOGV("setVideoTarget");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        sp<IBinder> b(IInterface::asBinder(bufferProducer));
        data.writeStrongBinder(b);
        remote()->transact(SET_VIDEO_BUFFER_TARGET, data, &reply);
        return reply.readInt32();
    }

    status_t awCamRecInit(const String8& filename, int format,
                                      int width, int height, int framerate, int bitrate,
                                      int audiomode, int audiobitrate)
    {
        ALOGV("Camera awCamRecInit filename:%s, f:%d, w:%d, h:%d, fr:%d, br:%d, am:%d, abr:%d",
            filename.string(), format, width, height, framerate, bitrate, audiomode, audiobitrate);
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeString8(filename);
        data.writeInt32(format);
        data.writeInt32(width);
        data.writeInt32(height);
        data.writeInt32(framerate);
        data.writeInt32(bitrate);
        data.writeInt32(audiomode);
        data.writeInt32(audiobitrate);
        remote()->transact(AW_CAM_REC_INIT, data, &reply);
        return reply.readInt32();
    }

    status_t awCamRecStart()
    {
        ALOGV("Camera awCamRecStart!");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(AW_CAM_REC_START, data, &reply);
        return reply.readInt32();
    }

    status_t awCamRecStop()
    {
        ALOGV("Camera awCamRecStop!");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(AW_CAM_REC_STOP, data, &reply);
        return reply.readInt32();
    }

    status_t awCamRecSetParaAndCmds(const String8& parameters, int cmds)
    {
        ALOGV("Camera awCamRecSetParaAndCmds parameters = %s, cmds = %d",parameters.string(),cmds);
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeString8(parameters);
        data.writeInt32(cmds);
        remote()->transact(AW_CAM_REC_SET_PARA_AND_CMDS, data, &reply);
        return reply.readInt32();
    }

    String8 awCamRecGetParaAndCmds(int cmds) const
    {
        ALOGV("Camera awCamRecGetParaAndCmds cmds = %d",cmds);
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(cmds);
        remote()->transact(AW_CAM_REC_GET_PARA_AND_CMDS, data, &reply);
        return reply.readString8();
    }

    status_t awCamRecStartNextFile(const String8& filename)
    {
        ALOGV("Camera awCamRecStartNextFile parameters = %s",filename.string());
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeString8(filename);
        remote()->transact(AW_CAM_REC_START_NEXT_FILE, data, &reply);
        return reply.readInt32();
    }

    status_t awCamRecRelease()
    {
        ALOGV("Camera awCamRecRelease!");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(AW_CAM_REC_RELEASE, data, &reply);
        return reply.readInt32();
    }

    void startRender()
    {
        ALOGV("Camera startRender!");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(AW_CAM_START_RENDER, data, &reply);
    }

    void stopRender()
    {
        ALOGV("Camera stopRender!");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(AW_CAM_STOP_RENDER, data, &reply);
    }

    status_t startWaterMark()
    {
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(AW_START_WATER_MARK, data, &reply);
        status_t ret = reply.readInt32();
        ALOGV("startWaterMark ret  = %d",ret);
        return ret;
    }

    status_t stopWaterMark()
    {
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(AW_STOP_WATER_MARK, data, &reply);
        status_t ret = reply.readInt32();
        ALOGV("stopWaterMark ret  = %d",ret);
        return ret;
    }

    status_t setWaterMarkMultiple(const String8& mWaterMark, int dispMode)
    {
        ALOGV("setWaterMarkMultiple");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeString8(mWaterMark);
        data.writeInt32(dispMode);
        remote()->transact(SET_WATER_MARK_MULTIPLE, data, &reply);
        status_t ret = reply.readInt32();
        ALOGV("setWaterMarkMultiple ret  = %d",ret);
        return ret;
    }

    status_t setEGLPreviewTarget(const sp<IGraphicBufferProducer>& bufferProducer)
    {
        ALOGV("setEGLPreviewTarget");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        sp<IBinder> b(IInterface::asBinder(bufferProducer));
        data.writeStrongBinder(b);
        remote()->transact(SET_EGL_PREVIEW_TARGET, data, &reply);
        return reply.readInt32();
    }
    status_t startEGLPreview()
    {
        ALOGV("startEGLPreview");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(START_EGL_PREVIEW, data, &reply);
        return reply.readInt32();
    }

    status_t stopEGLPreview()
    {
        ALOGV("stopEGLPreview");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(STOP_EGL_PREVIEW, data, &reply);
        return reply.readInt32();
    }

    status_t setCarSize(float length,float width,float height)
    {
        ALOGV("setCarSize");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeFloat(length);
        data.writeFloat(width);
        data.writeFloat(height);
        remote()->transact(SET_CAR_SIZE, data, &reply);
        return reply.readInt32();
    }

    status_t setScreenSize(int width,int height)
    {
        ALOGV("setScreenSize");
        Parcel data,reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(width);
        data.writeInt32(height);
        remote()->transact(SET_SCREEN_SIZE,data,&reply);
        return reply.readInt32();
    }

    status_t setCurrentPoint(int pointX,int pointY)
    {
        ALOGV("setCurrentPoint");
        Parcel data,reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(pointX);
        data.writeInt32(pointY);
        remote()->transact(SET_CURRENT_POINT,data,&reply);
        return reply.readInt32();
    }

    status_t manualAdjust(int num,int *pPoint)
    {
        ALOGV("manualAdjust");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(num);
        for(int i=0;i<num*2;i++)
        {
            data.writeInt32(pPoint[i]);
        }
        remote()->transact(MANUAL_ADJUST, data, &reply);
        return reply.readInt32();
    }

    status_t setTouchData(int type,int count,int *jamoveDis,int scaleDis)
    {
        ALOGV("setTouchData");
        Parcel data, reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(type);
        data.writeInt32(count);
        for(int i=0;i<count;i++)
        {
            data.writeInt32(jamoveDis[i]);
        }
        data.writeInt32(scaleDis);
        remote()->transact(SET_TOUCH_DATA, data, &reply);
        return reply.readInt32();
    }

    status_t setMaxWheelAngleInfo(int maxSteeringAngle,int maxWheelAngle)
    {
        ALOGV("setMaxWheelAngleInfo");
        Parcel data,reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(maxSteeringAngle);
        data.writeInt32(maxWheelAngle);
        remote()->transact(SET_MAX_WHEEL_INFO,data,&reply);
        return reply.readInt32();
    }

    status_t setCameraPosition(Camera_RVCameraPosition *position)
    {
        ALOGV("setCameraPosition");
        Parcel data,reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeFloat(position->disFront);
        data.writeFloat(position->disBack);
        data.writeFloat(position->disMirrorToFront);
        for(int i=0;i<4;i++)
        {
            data.writeFloat(position->heightCam[i]);
        }
        remote()->transact(SET_CAMERA_POSITION,data,&reply);
        return reply.readInt32();
    }

    status_t getCameraPosition(Camera_RVCameraPosition *position)
    {
        ALOGV("getCameraPosition");
        Parcel data,reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(GET_CAMERA_POSITION,data,&reply);
        position->disFront = reply.readFloat();
        position->disBack = reply.readFloat();
        position->disMirrorToFront = reply.readFloat();
        for(int i=0;i<4;i++)
        {
            position->heightCam[i]= reply.readFloat();
        }
        return 0;
    }

    status_t setStructureInfo(Camera_RVCarStructureInfo *info)
    {
        ALOGV("setCameraPosition");
        Parcel data,reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeFloat(info->wheelBase);
        data.writeFloat(info->treadFront);
        data.writeFloat(info->disBackWheelToBack);

        remote()->transact(SET_STRUCT_INFO,data,&reply);
        return reply.readInt32();
    }

    status_t getStructureInfo(Camera_RVCarStructureInfo *info)
    {
        ALOGV("getStructureInfo");
        Parcel data,reply;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(GET_STRUCT_INFO,data,&reply);
        info->wheelBase = reply.readFloat();
        info->treadFront = reply.readFloat();
        info->disBackWheelToBack = reply.readFloat();
        return 0;
    }
    status_t getRadarInfo(Camera_RVRadarAllInfos *info)
    {
        ALOGV("getRadarInfo");
        Parcel data,reply;
        int i;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        remote()->transact(GET_RADAR_INFO,data,&reply);
        info->numRadars = reply.readInt32();
        if(info->numRadars > 0)
        {
            for(i=0;i<info->numRadars;i++)
            {
                info->info[i].position[0] = reply.readFloat();
                info->info[i].position[1] = reply.readFloat();
                info->info[i].angle = reply.readFloat();
                info->info[i].range = reply.readFloat();
            }
        }
        return 0;

    }
    status_t setRadarInfo(Camera_RVRadarAllInfos *info)
    {
        ALOGV("setRadarInfo");
        Parcel data,reply;
        int i;
        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(info->numRadars);
        for(i=0;i<info->numRadars;i++)
        {
            data.writeFloat(info->info[i].position[0]);
            data.writeFloat(info->info[i].position[1]);
            data.writeFloat(info->info[i].angle);
            data.writeFloat(info->info[i].range);
        }
        remote()->transact(SET_RADAR_INFO,data,&reply);
        return reply.readInt32();
    }

    status_t setRadarData(float *pData,int count)
    {
        ALOGV("setRadarData");
        Parcel data,reply;
        int i;

        data.writeInterfaceToken(ICamera::getInterfaceDescriptor());
        data.writeInt32(count);
        for(i=0;i<count;i++)
        {
            data.writeFloat(pData[i]);
        }
        remote()->transact(SET_RADAR_DATA,data,&reply);
        return reply.readInt32();
    }
};

IMPLEMENT_META_INTERFACE(Camera, "android.hardware.ICamera");

// ----------------------------------------------------------------------

status_t BnCamera::onTransact(
    uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
    switch(code) {
        case DISCONNECT: {
            ALOGV("DISCONNECT");
            CHECK_INTERFACE(ICamera, data, reply);
            disconnect();
            reply->writeNoException();
            return NO_ERROR;
        } break;
        case SET_PREVIEW_TARGET: {
            ALOGV("SET_PREVIEW_TARGET");
            CHECK_INTERFACE(ICamera, data, reply);
            sp<IGraphicBufferProducer> st =
                interface_cast<IGraphicBufferProducer>(data.readStrongBinder());
            reply->writeInt32(setPreviewTarget(st));
            return NO_ERROR;
        } break;
        case SET_PREVIEW_CALLBACK_FLAG: {
            ALOGV("SET_PREVIEW_CALLBACK_TYPE");
            CHECK_INTERFACE(ICamera, data, reply);
            int callback_flag = data.readInt32();
            setPreviewCallbackFlag(callback_flag);
            return NO_ERROR;
        } break;
        case SET_PREVIEW_CALLBACK_TARGET: {
            ALOGV("SET_PREVIEW_CALLBACK_TARGET");
            CHECK_INTERFACE(ICamera, data, reply);
            sp<IGraphicBufferProducer> cp =
                interface_cast<IGraphicBufferProducer>(data.readStrongBinder());
            reply->writeInt32(setPreviewCallbackTarget(cp));
            return NO_ERROR;
        }
        case START_PREVIEW: {
            ALOGV("START_PREVIEW");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(startPreview());
            return NO_ERROR;
        } break;
        case START_RECORDING: {
            ALOGV("START_RECORDING");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(startRecording());
            return NO_ERROR;
        } break;
        case STOP_PREVIEW: {
            ALOGV("STOP_PREVIEW");
            CHECK_INTERFACE(ICamera, data, reply);
            stopPreview();
            return NO_ERROR;
        } break;
        case STOP_RECORDING: {
            ALOGV("STOP_RECORDING");
            CHECK_INTERFACE(ICamera, data, reply);
            stopRecording();
            return NO_ERROR;
        } break;
        case RELEASE_RECORDING_FRAME: {
            ALOGV("RELEASE_RECORDING_FRAME");
            CHECK_INTERFACE(ICamera, data, reply);
            sp<IMemory> mem = interface_cast<IMemory>(data.readStrongBinder());
            releaseRecordingFrame(mem);
            return NO_ERROR;
        } break;
        case RELEASE_RECORDING_FRAME_HANDLE: {
            ALOGV("RELEASE_RECORDING_FRAME_HANDLE");
            CHECK_INTERFACE(ICamera, data, reply);
            // releaseRecordingFrameHandle will be responsble to close the native handle.
            releaseRecordingFrameHandle(data.readNativeHandle());
            return NO_ERROR;
        } break;
        case RELEASE_RECORDING_FRAME_HANDLE_BATCH: {
            ALOGV("RELEASE_RECORDING_FRAME_HANDLE_BATCH");
            CHECK_INTERFACE(ICamera, data, reply);
            // releaseRecordingFrameHandle will be responsble to close the native handle.
            uint32_t n = data.readUint32();
            std::vector<native_handle_t*> handles;
            handles.reserve(n);
            for (uint32_t i = 0; i < n; i++) {
                handles.push_back(data.readNativeHandle());
            }
            releaseRecordingFrameHandleBatch(handles);
            return NO_ERROR;
        } break;
        case SET_VIDEO_BUFFER_MODE: {
            ALOGV("SET_VIDEO_BUFFER_MODE");
            CHECK_INTERFACE(ICamera, data, reply);
            int32_t mode = data.readInt32();
            reply->writeInt32(setVideoBufferMode(mode));
            return NO_ERROR;
        } break;
        case PREVIEW_ENABLED: {
            ALOGV("PREVIEW_ENABLED");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(previewEnabled());
            return NO_ERROR;
        } break;
        case RECORDING_ENABLED: {
            ALOGV("RECORDING_ENABLED");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(recordingEnabled());
            return NO_ERROR;
        } break;
        case AUTO_FOCUS: {
            ALOGV("AUTO_FOCUS");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(autoFocus());
            return NO_ERROR;
        } break;
        case CANCEL_AUTO_FOCUS: {
            ALOGV("CANCEL_AUTO_FOCUS");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(cancelAutoFocus());
            return NO_ERROR;
        } break;
        case TAKE_PICTURE: {
            ALOGV("TAKE_PICTURE");
            CHECK_INTERFACE(ICamera, data, reply);
            int msgType = data.readInt32();
            reply->writeInt32(takePicture(msgType));
            return NO_ERROR;
        } break;
        case SET_PARAMETERS: {
            ALOGV("SET_PARAMETERS");
            CHECK_INTERFACE(ICamera, data, reply);
            String8 params(data.readString8());
            reply->writeInt32(setParameters(params));
            return NO_ERROR;
         } break;
        case GET_PARAMETERS: {
            ALOGV("GET_PARAMETERS");
            CHECK_INTERFACE(ICamera, data, reply);
             reply->writeString8(getParameters());
            return NO_ERROR;
         } break;
        case SEND_COMMAND: {
            ALOGV("SEND_COMMAND");
            CHECK_INTERFACE(ICamera, data, reply);
            int command = data.readInt32();
            int arg1 = data.readInt32();
            int arg2 = data.readInt32();
            reply->writeInt32(sendCommand(command, arg1, arg2));
            return NO_ERROR;
         } break;
        case CONNECT: {
            CHECK_INTERFACE(ICamera, data, reply);
            sp<ICameraClient> cameraClient = interface_cast<ICameraClient>(data.readStrongBinder());
            reply->writeInt32(connect(cameraClient));
            return NO_ERROR;
        } break;
        case LOCK: {
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(lock());
            return NO_ERROR;
        } break;
        case UNLOCK: {
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(unlock());
            return NO_ERROR;
        } break;
        case SET_VIDEO_BUFFER_TARGET: {
            ALOGV("SET_VIDEO_BUFFER_TARGET");
            CHECK_INTERFACE(ICamera, data, reply);
            sp<IGraphicBufferProducer> st =
                interface_cast<IGraphicBufferProducer>(data.readStrongBinder());
            reply->writeInt32(setVideoTarget(st));
            return NO_ERROR;
        } break;
        case AW_CAM_REC_INIT: {
            ALOGV("AW_CAM_REC_INIT");
            CHECK_INTERFACE(ICamera, data, reply);
            String8 filename(data.readString8());
            int format = data.readInt32();
            int width = data.readInt32();
            int height = data.readInt32();
            int framerate = data.readInt32();
            int bitrate = data.readInt32();
            int audiomode = data.readInt32();
            int audiobitrate = data.readInt32();
            reply->writeInt32(awCamRecInit(filename, format, width, height,
                framerate, bitrate, audiomode, audiobitrate));
            return NO_ERROR;
        }break;
        case AW_CAM_REC_START: {
            ALOGV("AW_CAM_REC_START");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(awCamRecStart());
            return NO_ERROR;
        }break;
        case AW_CAM_REC_STOP: {
            ALOGV("AW_CAM_REC_STOP");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(awCamRecStop());
            return NO_ERROR;
        }break;
        case AW_CAM_REC_SET_PARA_AND_CMDS: {
            ALOGV("AW_CAM_REC_SET_PARA_AND_CMDS");
            CHECK_INTERFACE(ICamera, data, reply);
            String8 parameters(data.readString8());
            int cmds = data.readInt32();
            reply->writeInt32(awCamRecSetParaAndCmds(parameters,cmds));
            return NO_ERROR;
        }break;
        case AW_CAM_REC_GET_PARA_AND_CMDS: {
            ALOGV("AW_CAM_REC_GET_PARA_AND_CMDS");
            CHECK_INTERFACE(ICamera, data, reply);
            int cmds = data.readInt32();
            reply->writeString8(awCamRecGetParaAndCmds(cmds));
            return NO_ERROR;
        }break;
        case AW_CAM_REC_START_NEXT_FILE: {
            ALOGV("AW_CAM_REC_START_NEXT_FILE");
            CHECK_INTERFACE(ICamera, data, reply);
            String8 filename(data.readString8());
            reply->writeInt32(awCamRecStartNextFile(filename));
            return NO_ERROR;
        }break;
        case AW_CAM_REC_RELEASE: {
            ALOGV("AW_CAM_REC_RELEASE");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(awCamRecRelease());
            return NO_ERROR;
        }break;
        case AW_CAM_START_RENDER: {
            ALOGV("AW_CAM_REC_RELEASE");
            CHECK_INTERFACE(ICamera, data, reply);
            startRender();
            return NO_ERROR;
        }break;
        case AW_CAM_STOP_RENDER: {
            ALOGV("AW_CAM_REC_RELEASE");
            CHECK_INTERFACE(ICamera, data, reply);
            stopRender();
            return NO_ERROR;
        }break;
        case AW_START_WATER_MARK: {
            ALOGV("AW_START_WATER_MARK");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(startWaterMark());
            return NO_ERROR;
        } break;
        case AW_STOP_WATER_MARK: {
            ALOGV("AW_STOP_WATER_MARK");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(stopWaterMark());
            return NO_ERROR;
        } break;
        case SET_WATER_MARK_MULTIPLE: {
            ALOGV("SET_WATER_MARK_MULTIPLE");
            CHECK_INTERFACE(ICamera, data, reply);
            String8 mWaterMark(data.readString8());
            int dispMode = data.readInt32();
            reply->writeInt32(setWaterMarkMultiple(mWaterMark,dispMode));
            return NO_ERROR;
        } break;
        case SET_EGL_PREVIEW_TARGET: {
            ALOGV("SET_EGL_PREVIEW_TARGET");
            CHECK_INTERFACE(ICamera, data, reply);
            sp<IGraphicBufferProducer> st =
                interface_cast<IGraphicBufferProducer>(data.readStrongBinder());
            reply->writeInt32(setEGLPreviewTarget(st));
            return NO_ERROR;
        } break;
        case START_EGL_PREVIEW: {
            ALOGV("START_EGL_PREVIEW");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(startEGLPreview());
            return NO_ERROR;
        } break;
        case STOP_EGL_PREVIEW: {
            ALOGV("STOP_EGL_PREVIEW");
            CHECK_INTERFACE(ICamera, data, reply);
            reply->writeInt32(stopEGLPreview());
            return NO_ERROR;
        } break;
        case SET_CAR_SIZE:{
            ALOGV("SET_CAR_SIZE");
            CHECK_INTERFACE(ICamera,data,reply);
            float length = data.readFloat();
            float width = data.readFloat();
            float height = data.readFloat();
            reply->writeInt32(setCarSize(length,width,height));
            return NO_ERROR;
        }break;
        case SET_SCREEN_SIZE:{
            ALOGV("SET_SCREEN_SIZE");
            CHECK_INTERFACE(ICamera,data,reply);
            int32_t width = data.readInt32();
            int32_t height = data.readInt32();
            reply->writeInt32(setScreenSize(width,height));
            return NO_ERROR;
        }break;
        case SET_CURRENT_POINT:{
            ALOGV("SET_CURRENT_POINT");
            CHECK_INTERFACE(ICamera,data,reply);
            int32_t pointX = data.readInt32();
            int32_t pointY = data.readInt32();
            reply->writeInt32(setCurrentPoint(pointX,pointY));
            return NO_ERROR;
        }break;
        case MANUAL_ADJUST:{
            ALOGV("MANUAL_ADJUST");
            CHECK_INTERFACE(ICamera,data,reply);
            int32_t num = data.readInt32();
            int pPoint[128] ={0};
            ALOGD("MANUAL_ADJUST1 num=%d",num);

            for(int i=0;i<num*2;i++)
            {
                pPoint[i] = data.readInt32();
                ALOGD("MANUAL_ADJUST pPoint1[%d]=%d",i,pPoint[i]);
            }
            reply->writeInt32(manualAdjust(num,pPoint));

            ALOGD("MANUAL_ADJUST1 num=%d end",num);
            return NO_ERROR;
        }break;
        case SET_TOUCH_DATA: {
            ALOGV("STOP_EGL_PREVIEW");
            CHECK_INTERFACE(ICamera, data, reply);
            int *jamoveDis = NULL;
            int32_t type = data.readInt32();
            int32_t count = data.readInt32();
            if(count > 0)
            {
                jamoveDis = (int *)malloc(count);
            }
            for(int i=0;i<count;i++)
            {
                jamoveDis[i] = data.readInt32();
                ALOGD("MANUAL_ADJUST jamoveDis[%d]=%d",i,jamoveDis[i]);
            }
            int32_t scaleDis = data.readInt32();
            reply->writeInt32(setTouchData(type,count,jamoveDis,scaleDis));
            if(count > 0)
            {
                free(jamoveDis);
            }
            return NO_ERROR;
        } break;
        case SET_MAX_WHEEL_INFO:{
            ALOGV("SET_MAX_WHEEL_INFO");
            CHECK_INTERFACE(ICamera,data,reply);
            int32_t maxSteeringAngle = data.readInt32();
            int32_t maxWheelAngle = data.readInt32();
            reply->writeInt32(setMaxWheelAngleInfo(maxSteeringAngle,maxWheelAngle));
            return NO_ERROR;
        }break;
        case SET_CAMERA_POSITION:{
            ALOGV("SET_CAMERA_POSITION");
            CHECK_INTERFACE(ICamera,data,reply);
            Camera_RVCameraPosition position;
            position.disFront = data.readFloat();
            position.disBack = data.readFloat();
            position.disMirrorToFront = data.readFloat();
            for(int i=0;i<4;i++)
            {
                position.heightCam[i] = data.readFloat();
            }
            reply->writeInt32(setCameraPosition(&position));
            return NO_ERROR;
        }break;
        case GET_CAMERA_POSITION:{
            ALOGV("GET_CAMERA_POSITION");
            CHECK_INTERFACE(ICamera,data,reply);
            Camera_RVCameraPosition position;
            getCameraPosition(&position);
            reply->writeFloat(position.disFront);
            reply->writeFloat(position.disBack);
            reply->writeFloat(position.disMirrorToFront);
            for(int i=0;i<4;i++)
            {
                reply->writeFloat(position.heightCam[i]);
            }
            return NO_ERROR;
        }break;
        case SET_STRUCT_INFO:{
            ALOGV("SET_STRUCT_INFO");
            CHECK_INTERFACE(ICamera,data,reply);
            Camera_RVCarStructureInfo info;
            info.wheelBase = data.readFloat();
            info.treadFront = data.readFloat();
            info.disBackWheelToBack = data.readFloat();
            reply->writeInt32(setStructureInfo(&info));
            return NO_ERROR;
        }break;
        case GET_STRUCT_INFO:{
            ALOGV("SET_CAMERA_POSITION");
            CHECK_INTERFACE(ICamera,data,reply);
            Camera_RVCarStructureInfo info;
            getStructureInfo(&info);
            reply->writeFloat(info.wheelBase);
            reply->writeFloat(info.treadFront);
            reply->writeFloat(info.disBackWheelToBack);

            return NO_ERROR;
        }break;
        case GET_RADAR_INFO:{
            ALOGV("GET_RADAR_INFO");
            CHECK_INTERFACE(ICamera,data,reply);
            Camera_RVRadarAllInfos info;
            int i;
            getRadarInfo(&info);
            reply->writeInt32(info.numRadars);
            for(i=0;i<info.numRadars;i++)
            {
                reply->writeFloat(info.info[i].position[0]);
                reply->writeFloat(info.info[i].position[1]);
                reply->writeFloat(info.info[i].angle);
                reply->writeFloat(info.info[i].range);
            }
            return NO_ERROR;
        }break;
        case SET_RADAR_INFO:{
            ALOGD("SET_RADAR_INFO");
            CHECK_INTERFACE(ICamera,data,reply);
            Camera_RVRadarAllInfos info;
            int i;

            info.numRadars = data.readInt32();
            for(i=0;i<info.numRadars;i++)
            {
                info.info[i].position[0] = data.readFloat();
                info.info[i].position[1] = data.readFloat();
                info.info[i].angle = data.readFloat();
                info.info[i].range = data.readFloat();
            }
            reply->writeInt32(setRadarInfo(&info));
            return NO_ERROR;
        }break;
        case SET_RADAR_DATA:{
            ALOGV("SET_RADAR_DATA");
            CHECK_INTERFACE(ICamera,data,reply);
            float radarData[21];
            int count;
            int i;

            count = data.readInt32();
            if(count >=20)
                count = 20;
            for(i=0;i<count;i++)
            {
                radarData[i] = data.readFloat();
            }
            reply->writeInt32(setRadarData(radarData,count));
            return NO_ERROR;
        }break;
        default:
            return BBinder::onTransact(code, data, reply, flags);
    }
}

// ----------------------------------------------------------------------------

} // namespace hardware
} // namespace android
