// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package cameraservice

import (
    "android/soong/android"
    "android/soong/cc"
    "fmt"
)

func init() {
    //fmt.Println("init camerasercie.go")
    android.RegisterModuleType("cameraservice_product_defaults", productDefaultsFactory)
}

func productDefaultsFactory() android.Module {
    fmt.Println("init productDefaultsFactory")
    module := cc.DefaultsFactory()
    android.AddLoadHook(module, productDefaults)

    return module
}

func productDefaults(ctx android.LoadHookContext) {
    type props struct {
        Cflags       []string
    }
    p := &props{}
    p.Cflags = globalDefaults(ctx)

    ctx.AppendProperties(p)
}

/* How to get Makefile var
 * 1. $(call soong_config_add,<namespace>,<keyname>,<keyval>) in BoardConfig.mk
 * 2. in your go file:
 *    var keyval string
 *    keyval = ctx.Config().VendorConfig("<namespace>").String("<keyname>")
 */
func globalDefaults(ctx android.BaseContext) ([]string) {
    var cppflags []string
    
    fmt.Println("init globalDefaults")
    if ctx.AConfig().VendorConfig("vendor").String("platform") != "" {
        var keyval string
        keyval = ctx.AConfig().VendorConfig("vendor").String("platform")
        if keyval == "homlet" {
            cppflags = append(cppflags,"-DTARGET_PLATFORM_HOMLET")
        } else if keyval == "auto" {
            cppflags = append(cppflags,"-DTARGET_PLATFORM_AUTO")
        } else {
            cppflags = append(cppflags,"-DTARGET_PLATFORM_TABLE")
        }
    }
    return cppflags
}
