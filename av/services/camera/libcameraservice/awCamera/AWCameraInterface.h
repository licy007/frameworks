//AWCameraInterface.h
//add by fengyun 20190923

#ifndef __AW_CAMERA_INTERFACE__
#define __AW_CAMERA_INTERFACE__
#include <system/window.h>
#include <binder/IMemory.h>
#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <camera/CameraParameters.h>
#include <utils/RefBase.h>
#include "AWCamRecorder/AWCamRecorder.h"
#include <system/camera.h>
#include "memory/AWmemoryAdapter.h"
#include "G2dApi.h"
#include <hardware/camera.h>

#define NB_BUFFER 6 //same with camera hal
#define MAX_CAMERA_NUM 4//the end of camera just for split camera
namespace android{

class AWCameraPreview;
class PictureManager;
class AWCameraInterface :public virtual RefBase{

public:
    AWCameraInterface(int cameraId,int cameraNum,int width,int height,
    int is360View,awCamReleaseCallback releaseCallback,notify_callback notifyCallback,void *user);
    virtual ~AWCameraInterface();
    int setPreviewWindow(const sp<ANativeWindow>& window);
    int stopPreview();
    int startPreview();
    int takePicture();
    int onCameraDataCallback(const sp<IMemory>& dataPtr);
    int  awCamRecInit(const String8& filename, int format,
                        int width, int height, int framerate, int bitrate,
                        int audiomode, int audiobitrate);
    int awCamRecStart();
    int awCamRecStop();
    int awCamRecSetParaAndCmds(const String8& parameters, int cmds);
    String8 awCamRecGetParaAndCmds(int cmds) const;
    int awCamRecStartNextFile(const String8& filename);
    int awCamRecRelease();
    void startRender();
    void stopRender();
    status_t startWaterMark();
    status_t stopWaterMark();
    status_t setWaterMarkMultiple(const String8& mWaterMark, int dispMode);
    static void __awCamRecReleaseFrame(void *owner,int index,int sharefd);
    static void __awCamPicReleaseFrame(void *owner,int index,int sharefd);
    static void __awCamNotifyCallback(int32_t msg_type, int32_t ext1,
                        int32_t ext2, void *user);
    int releaseCameraFrame(int index,int sharefd);
    bool releaseThread();
    status_t setEGLPreviewTarget(void *window);
    status_t startEGLPreview();
    status_t stopEGLPreview();
    status_t setCarSize(float length,float width,float height);
    status_t setScreenSize(int width,int height);
    status_t setCurrentPoint(int pointX,int pointY);
    status_t manualAdjust(int num,int * pPoint);
    status_t setTouchData(int type,int count,int *jamoveDis,int scaleDis);
    status_t  setMaxWheelAngleInfo(int maxSteeringAngle,int maxWheelAngle);
    status_t setCameraPosition(Camera_RVCameraPosition *position);
    status_t getCameraPosition(Camera_RVCameraPosition *position);
    status_t setStructureInfo(Camera_RVCarStructureInfo *info);
    status_t getStructureInfo(Camera_RVCarStructureInfo *info);
    status_t setRadarInfo(Camera_RVRadarAllInfos *info);
    status_t getRadarInfo(Camera_RVRadarAllInfos *info);
    status_t setRadarData(float *pData,int count);
    static int bv360SetVal(int32_t type, int32_t value);
    static int bv360GetVal(int32_t type);
    static int bv360SetStr(int32_t type, const String8& params);
    static String8 bv360GetStr(int type);
    bool handleThread();
    int setParameters(const CameraParameters &params);
    class DoHandleThread : public Thread {
            AWCameraInterface*    mCameraInterface;
            bool                mRequestExit;
        public:
            DoHandleThread(AWCameraInterface* dev) :
                Thread(false),
                mCameraInterface(dev),
                mRequestExit(false) {
            }
            void startThread() {
                run("HandleThread", PRIORITY_URGENT_DISPLAY);
            }
            void stopThread() {
                mRequestExit = true;
            }
            virtual bool threadLoop() {
                if (mRequestExit) {
                    return false;
                }
                return mCameraInterface->handleThread();
            }
        };
private:
    void setSnapPath(const char * str);
    void setPreviewSize(int width,int height);
    void setWaterMarkDispMode(int dispMode);
    void addWaterMark(unsigned char *addrVirY,int width, int height);
private:
    pthread_mutex_t mReleaseMutex;
    pthread_cond_t mReleaseCond;
    pthread_mutex_t mHandleMutex;
    pthread_cond_t mHandleCond;
    OSAL_QUEUE  mQueueBufferPreview;
    sp<DoHandleThread> mHandleThread;
    int mPreviewState;
    struct AWScCamMemOpsS* mMemOpsS;
    G2dOpsS* mG2dOpss;
private:
    int mCameraId;
    int mCameraNum;
    int mPreviewWidth;
    int mPreviewHeight;
    int mIs360View;
    AWCamRecorder* mAWCamRecorder;
    Mutex mAWCamLock;
    Mutex mWaterLock;
    sp<AWCameraPreview> mAWCameraPreview;
    PictureManager *mPictureManager;
    awCamReleaseCallback mReleaseCallback;
    notify_callback mNotifyCallback;
    void *mUser;
    V4L2BUF_t mCameraBuf[MAX_CAMERA_NUM+1][NB_BUFFER];
    bool mEnableWaterMark;
    void *mWaterMarkCtrlRec;
    void *mWaterMarkCtrlPrev;
    void *mWaterMarkMultiple;
    char mWaterMarkOther[128];
    int mWaterMarkDispMode;
    int mOviewPicIndex;
};
}
#endif //__AW_CAMERA_PREVIEW__
