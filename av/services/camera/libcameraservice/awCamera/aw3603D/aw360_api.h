#ifndef AW360_API_H
#define AW360_API_H
#include "G2dApi.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef int (*Func)(int command,int value,const char *params);
typedef char *(*FuncExt)(int command,int value,const char *params);
typedef int (*BirdviewCallBack)(int command, int flag,int ext1,int ext2,int ext3,void *cookie);
typedef int (*LockCallBack)(void *cookie);
typedef int (*FuncSet)(void);

typedef enum
{
    //add set command
    AW_COMMAND_SENSOR_TYPE = 0x0,    //bv360SetVal
	AW_COMMAND_LENS_TYPE = 0x01,         //bv360SetVal
    AW_COMMAND_CAR_SIZE = 0x02,      //// int setCarSize(float length,float width,float height);
    AW_COMMAND_SCREEN_SIZE = 0x03,   //int setScreenSize(int width,int height);
	AW_COMMAND_AUTO_CALIB = 0x04, 		 //bv360SetVal
	AW_COMMAND_CURRENT_POINT = 0x05, 		//int setCurrentPoint(int pointX,int pointY);
	AW_COMMAND_MANUAL_CALIB = 0x06, 		//int manualAdjust(int num ,int[] points);
    AW_COMMAND_CALIB_PATTERN_TYPE = 0x07, 	//bv360SetVal
    AW_COMMAND_SIDE_OFFSET = 0x08,          //bv360SetVal
	AW_COMMAND_CAMERA_OFFSET_X = 0x09,      //bv360SetVal
	AW_COMMAND_CAMERA_OFFSET_Y = 0x0a,      //bv360SetVal
	AW_COMMAND_CAMERA_SCALE = 0x0b, 		//bv360SetVal
	AW_COMMAND_2D_ADJUST_AREA = 0x0c, 	    //bv360SetVal
    AW_COMMAND_CAR_SPEED = 0x0d,            //bv360SetVal
    AW_COMMAND_STEERING_ANGLE = 0x0e,       //bv360SetVal
    AW_COMMAND_DISPLAY_MODE = 0x0f,         //bv360SetVal
    AW_COMMAND_DISPLAY_TEMPLATE = 0x10,     //bv360SetVal
    AW_COMMAND_TOUCH_DATA = 0x11,      //int setTouchData(int type,int[] moveDis,int scaleDis);
    AW_COMMAND_SHOW_CAR_SPEED = 0x12,       //bv360SetVal
    AW_COMMAND_SHOW_WHEEL_ANGLE = 0x13,    //bv360SetVal
    AW_COMMAND_SHOW_TURNING_LIGHTS = 0x14,  //bv360SetVal
    AW_COMMAND_SHOW_STEERING_VIEW = 0x15,   //bv360SetVal
    AW_COMMAND_SHOW_RADAR = 0x16,           //bv360SetVal
    AW_COMMAND_TRACE_TYPE = 0x17,           //bv360SetVal
    AW_COMMAND_CAR_MODEL_COLOR = 0x18,      //bv360SetVal
    AW_COMMAND_CAR_MODEL_TYPE = 0x19, //bv360SetVal
    AW_COMMAND_MAX_CAR_SPEED = 0x1A,//bv360SetVal
    AW_COMMAND_MAX_WHEEL_ANGLE_INFO = 0x1B,//int setMaxWheelAngleInfo(int maxSteeringAngle,int maxWheelAngle);
    AW_COMMAND_GET_MARKS = 0X1C,//AW NOT USE
    AW_COMMAND_FALLBACK = 0X1D,//bv360SetVal
    AW_COMMAND_ACTIVTION_ABANDON = 0x1E,//bv360SetStr and  bv360GetVal
    AW_COMMAND_SAVE_DATA = 0X1F, ////bv360SetVal
    AW_COMMAND_EXPORT_DATA =0X20,//bv360SetStr
    AW_COMMAND_IMPORT_DATA = 0X21,//bv360SetStr
    AW_COMMAND_GET_ACTIVTION_STATUS = 0X48,//bv360GetVal
    AW_COMMAND_GET_CHIPID = 0X4B,//bv360GetStr
    AW_COMMAND_ACTIVATE = 0X4C,//bv360SetStr
    AW_COMMAND_CAMERA_POSITION = 0X4D,//setCameraPosition getCameraPosition
    AW_COMMAND_CAR_STRUCT_INFO = 0X4E,//setStructureInfo getStructureInfo
    AW_COMMAND_RV_CMD_ANGLE_MODE  = 0X4F,//bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_EXTEND_FUNC = 0X50,//bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_SET_TRACE_STATE = 0x51,  //bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_LIGHT_STATE =0x52,//bv360SetVal bv360GetVal
    AW_COMMAND_DEBUG_SET_STR = 0x53,//bv360SetStr
    AW_COMMAND_RV_CMD_RADARS_INFO = 0x54,//setRadarInfo  getRadarInfo
    AW_COMMAND_RV_CMD_RADARS_DATA = 0X55,//setRadarData
    AW_COMMAND_GET_VERSION = 0x56,//bv360GetVal
	AW_COMMAND_RV_CMD_DOOR_STATE,//bv360GetVal bv360SetVal
    AW_COMMAND_RV_CMD_CAMERA_ROTATE,//bv360GetVal bv360SetVal
    AW_COMMAND_RV_CMD_CAMERA_BIAS_X,//bv360GetVal bv360SetVal
    AW_COMMAND_RV_CMD_CAMERA_BIAS_Y,//bv360GetVal bv360SetVal
    AW_COMMAND_RV_CMD_SHOW_RULER_LINE, //bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_CAMERA_SCALE_X, //bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_CAMERA_SCALE_Y,//bv360SetVal bv360GetVal

	AW_COMMAND_RV_CMD_CAMERA_OFFSET_Z,////bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_VIEW_ADJUST_SELECT,////bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_VIEW_RESET,////bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_BOWL_HEIGHT_ADJUST,////bv360SetVal bv360GetVal
    AW_COMMAND_RV_CMD_STORAGE_STATE,//bv360SetStr and  bv360GetVal
    AW_COMMAND_RV_CMD_BIRVIEW_REGIOND,//bv360SetStr and  bv360GetVal
}BIRDVIEW_COMMAND;

typedef struct _BIRDVIEW_PARAM_
{
    int command;
    Func funcPtr;
    FuncExt funcPtrExt;
}BIRDVIEW_PARAM;

typedef struct _BIRDVIEW_3DSET_
{
    int command;
    FuncSet funcPtr;
}BIRDVIEW_3DSET;

#define BIRDVIEW_PAL_WIDTH                  720
#define BIRDVIEW_PAL_HEIGHT                 576
#define BIRDVIEW_NTSC_WIDTH                 720
#define BIRDVIEW_NTSC_HEIGHT                480

#define BIRDVIEW_4PAL_WIDTH                 (BIRDVIEW_PAL_WIDTH*2)
#define BIRDVIEW_4PAL_HEIGHT                (BIRDVIEW_PAL_HEIGHT*2)
#define BIRDVIEW_4NTSC_WIDTH                (BIRDVIEW_NTSC_WIDTH*2)
#define BIRDVIEW_4NTSC_HEIGHT               (BIRDVIEW_NTSC_HEIGHT*2)

int birdviewEntryMulti(void *srcFront, void *srcRight,void *srcBack,void *srcLeft,
                            int inWidth, int inHeight, int inPitch, int flag, void * dstBuffer,int srcBackMirror);

int birdviewRecorderEntry(void *srcBuffer);
int birdviewRecorderEntryMulti(void *srcFront, void *srcRight,void *srcBack,void *srcLeft,
                            int inWidth, int inHeight,void * dstBuffer,int isScale);

int bvApiInit(BirdviewCallBack callback,void *user);
void bvSetProductType(int type);
int bvStopEGLPreview();
int bvApiExit();
int handleBirdView();
int bvSetWindow(void *window);
int birdViewSetVal(int command,int value,const char *param);
int birdViewGetVal(int command,int value,const char *param);
char *birdViewGetStr(int command,int value,const char *param);
int birdViewSetStr(int command,int value,const char *param);
int bvSetCarSize(float length,float width,float height);
int bvSetScreenSize(int width,int height);
int bvSetCurrentPoint(int pointX,int pointY);
int bvManualAdjust(int num,int *pPoint);
int bvSetTouchData(int type,int count,int *jamoveDis,int scaleDis);
int bvSetMaxWheelAngleInfo(int maxSteeringAngle,int maxWheelAngle);
int bvSetCameraPosition(void *position);
int bvGetCameraPosition(void *position);
int bvSetCarStructInfo(void *structInfo);
int bvGetCarStructInfo(void *structInfo);
int bvSetRadarInfo(void *info);
int bvGetRadarInfo(void *info);
int bvSetRadarData(float *data,int length);
void activeBirdView(void);
int birdViewUpdateParam(int command,int value,const char *param);
char *birdViewUpdateParamExt(int command,int value,const char *param);
int getBirdViewWorkMode();
#ifdef __cplusplus
}
#endif

#endif
