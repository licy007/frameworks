//EGLRender.h
//add by fengyun 20180731

#ifndef EGLRENDER_H
#define EGLRENDER_H
#include <utils/RefBase.h>
#include <binder/IInterface.h>
#include <binder/Parcel.h>
#include <binder/IMemory.h>
#include <binder/Status.h>
#include <utils/String8.h>
#include <pthread.h>
#include <EGL/egl.h> 
#include <GLES/gl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

namespace android {

class EGLRender {

public:
    EGLRender();
    virtual ~EGLRender();

    void start();
    void stop();
	void sync();
	void render();
    void setNativeWindow(const sp<ANativeWindow>& window);
    
    
private:

    enum RenderThreadMessage {
        MSG_NONE = 0,
        MSG_WINDOW_SET,
        MSG_RENDER_LOOP_EXIT
    };

    pthread_t mTreadId;
    pthread_mutex_t mMutex;
    enum RenderThreadMessage mRenderMsg;
    
    sp<ANativeWindow> mWindow;

    EGLDisplay mEGLDisplay;
    EGLSurface mEGLSurface;
    EGLContext mEGLContext;
    GLfloat mAngle;
    
    // RenderLoop is called in a rendering thread started in start() method
    // It creates rendering context and renders scene until stop() is called
    
    
    bool initialize();
    void destroy();
	void renderTest();
	void renderLoop();
    static void* ThreadRender(void *user);

};
}// namespace android
#endif // EGLRENDER_H
