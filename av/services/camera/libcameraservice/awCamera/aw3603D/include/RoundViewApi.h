/***********************************************************************
 *
 * This proprietary software may be used only as
 * authorised by a licensing agreement from the China ivicar Limited
 *
 * Author: Hongbo Zhong <hongbo.zhong@ivicar.cn>
 *
 * Copyright (c) 2017 IVICAR Corporation, Beijing, China.
 *               http://www.ivicar.cn/
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * 'Software'), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
 * IN NO EVENT SHALL VIVANTE AND/OR ITS SUPPLIERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *****************************************************************************/

#ifndef ROUNDVIEW_ROUNDVIEW_API_H
#define ROUNDVIEW_ROUNDVIEW_API_H

#include <stdint.h>
#include "G2dApi.h"
#include "RoundViewPrivate.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SYSTEM_CFG_PATH_SIZE 64

typedef enum _RoundViewCMD
{
	RV_CMD_SENSOR_TYPE = 0,     // 镜头数据类型，RVSensorType，默认值为0
	RV_CMD_LENS_TYPE,           // 去畸变表类型，RVLensType，默认值为1

    RV_CMD_CAR_SIZE,            // 汽车尺寸，float[3]，单位：米，默认值为(1.7,4.8,1.9)
    RV_CMD_SCREEN_SIZE,         // 屏幕宽高，uint32_t[2]，(width,height),默认值(1024,600)

	RV_CMD_AUTO_CALIB,		    // 自动校正，null
	RV_CMD_CURRENT_POINT = 5,		// 当前放大点，int[2]，点在图像中的XY坐标
	RV_CMD_MANUAL_CALIB,		// 手动校正，RVCameraPoints，所有镜头中的点
    RV_CMD_CALIB_PATTERN_TYPE,	// 标定模板类型，RVCalibPatternType，默认值为0

    RV_CMD_SIDE_OFFSET,         // 左右棋盘格与车前的距离，float，默认值为1.2（米），范围为[0.0,3.0]
	RV_CMD_CAMERA_OFFSET_X,     // 2D（原图/去畸变图）部分显示，X偏移量，int，+/-分别表示右移/左移
	RV_CMD_CAMERA_OFFSET_Y = 10,     // 2D（原图/去畸变图）部分显示，Y偏移量，int，+/-分别表示下移/上移
	RV_CMD_CAMERA_SCALE,		// 2D（原图/去畸变图）部分显示，水平和垂直方向相同的缩放量，int，+/-分别表示放大/缩小
	RV_CMD_2D_ADJUST_AREA,	    // 俯视视图范围-缩放系数增量，int, +/-分别表示放大/缩小

    RV_CMD_CAR_SPEED,           // 车速，int，正/负值分别表示前进/后退，范围[-maxSpeed, maxSpeed]
    RV_CMD_STEERING_ANGLE,      // 方向盘转角，int，正/负值分表表示左/右，范围[-maxSteeringWheel, maxSteeringWheel]
    
    RV_CMD_DISPLAY_MODE = 15,        // 显示模式-视角编号，RVDisplayMode
    RV_CMD_DISPLAY_TEMPLATE,    // 显示模板（布局），RVDisplayTemplate

    RV_CMD_TOUCH_DATA,          // 触摸操作，RVTouchData

    RV_CMD_SHOW_CAR_SPEED,      // 是否显示车速，int，默认值为false
    RV_CMD_SHOW_WHEEL_ANGLE,    // 是否显示转角，int，默认值为false
    RV_CMD_SHOW_TURNING_LIGHTS = 20, // 是否打开转向灯效果，int，默认值为false
    RV_CMD_SHOW_STEERING_VIEW,  // 是否打开方向盘与视角联动效果，int，默认值为false
    RV_CMD_SHOW_RADAR,          // 是否显示雷达，int，默认值为false
    RV_CMD_TRACE_TYPE,          // 轨迹线样式，RVTraceType，默认值为0
    RV_CMD_CAR_MODEL_COLOR,     // 车模型颜色，RVCarModelColor，默认值为0
    RV_CMD_CAR_MODEL_TYPE = 25,      // 车模型样式，RVCarModelType，默认值为0

    RV_CMD_MAX_CAR_SPEED,       // 最大车速，uint16_t，默认值为180
    RV_CMD_MAX_WHEEL_ANGLE_INFO,// 最大方向盘转角与最大车轮转角，RVWheelAngleInfo，默认值为（720,45）

    RV_CMD_GET_MARKS,           // 获取mark点

    RV_CMD_FALLBACK,            // 回退设置，使用初始化时传入的共享结构体指针所指向的数据

    RV_CMD_CAMERA_POSITION = 30,     // 相机位置信息 RVCameraPosition
    RV_CMD_CAR_STRUCTURE_INFO,  // 汽车结构信息 RVCarStructureInfo
    RV_CMD_ANGLE_MODE,          // 视角模式 RVCameraAngleMode
    RV_CMD_EXTEND_FUNC,         // 功能扩展命令 Set: RVADASSwitch, Get: RVADASState

    RV_CMD_SET_TRACE_STATE,     // 设置轨迹朝向 int, 0表示朝后，1表示朝前, -1表示不显示
    RV_CMD_LIGHT_STATE = 35,         // 车灯状态 RVLightState
    RV_CMD_RADARS_INFO,         // 雷达安装位置及雷达信息 RVRadarAllInfos
    RV_CMD_RADARS_DATA,         // 雷达数据 float[20]
    RV_CMD_PRODUCT_INFO,        // 厂商算法库和版本信息 int, [31:30]: 厂商信息, 0默认ivicar(www.ivicar.cn).
				                //	[29:24]: 产品信息, [23:16]: 主版本, [15:9]: 为次版本, [8:0] 为调试版本

    RV_CMD_SHOW_RULER_LINE,     // 是否显示标尺线，int，0表示不显示，非0表示显示

    RV_CMD_DOOR_STATE = 40,      // 设置车门开关状态
    RV_CMD_CAMERA_ROTATE,       // +/- 表示右旋转/左旋转, 其他值表示对应的视口
    RV_CMD_CAMERA_BIAS_X,	// +/- 表示随动视角的左移/右移
    RV_CMD_CAMERA_BIAS_Y,	// +/- 表示随动视角的上移/下移

    RV_CMD_CAMERA_OFFSET,       // 实际镜头安装位置与理想镜头安装位置之间在左右方向上的偏差，向左偏为正值，向右则为负值，float[4]

    RV_CMD_CAMERA_SCALE_X = 45,      // 2D（原图/去畸变图）部分显示，水平方向上的缩放量，int，+/-分别表示放大/缩小
    RV_CMD_CAMERA_SCALE_Y,      // 2D（原图/去畸变图）部分显示，垂直方向上的缩放量，int，+/-分别表示放大/缩小

    RV_CMD_SCREEN_RECTANGLE,    // 设置屏幕的位置和大小, RVScreenRect
    RV_CMD_CHANGE_RENDER_SCREENS,   // 改变渲染的屏幕（变动的时候调用），默认是渲染两个屏幕, RVScreenIndex

    RV_CMD_SAVE_CFG,		    // 保存拼接配置数据
    RV_CMD_BACKUP_CFG = 50,
    RV_CMD_RECOVER_CFG,

    RV_CMD_CAMERA_OFFSET_Z,         // 3D固定视角调节, Z偏移量, int，+/-分别表示上移/下移
    RV_CMD_VIEW_ADJUST_SELECT,      // 2D视图: 0 代表除俯视全景图外的第一部分, 1代表第二部分, int
				    // 3D视图: 0 代表相机的平移, 1代表观察点的平移
    RV_CMD_VIEW_RESET,              // 恢复当前视角默认值, int.
    RV_CMD_BOWL_HEIGHT_ADJUST = 55, // 碗装模型高度调节, 高度变化量, int.
    RV_CMD_STORAGE_STATE,           // 参数为字符串, 最大为64字节
				    // "CAMERA": 存储4路图像等,
				    // "CONFIG": 存储视图调节配置参数, 车类型配置参数.
				    // "VIEWMODE": 存储模式, 默认上电是2D模式还是3D模式.
				    // "IMPORT": 导入外部数据, 也是存储的一种方式
    RV_CMD_BIRVIEW_REGIOND,         // 俯视全景区域位置: 参数为字符串, 最大为32个字节. 例如: 23x23_30x50_7.

    RV_CMD_Amount
} RoundViewCMD;

typedef enum _RVLensType   //lens类型枚举
{
	RV_LensType_8255 = 0,
	RV_LensType_4052,
	RV_LensType_9005,
	RV_LensType_2706,
	RV_LensType_3011,
    RV_LensType_8296,
    RV_LensType_huayang,
    RV_LensType_xinli,
    RV_LensType_6045,
    RV_LensType_3019,
    RV_LensType_3308,

	RV_LensType_Amount
} RVLensType;

typedef enum _RVSensorType  //传感器类型枚举
{
	RV_SensorType_0130 = 0,
	RV_SensorType_1133,
	RV_SensorType_225,
	RV_SensorType_H65,
    RV_SensorType_1233,

	RV_SensorType_Amount
} RVSensorType;

/*RoundView display mode */
typedef enum _RVDisplayMode
{
    // 3D
	RV_Display_RoundView_Front = 0,     /* 前视图3D */
    RV_Display_RoundView_Back,          /* 后视图3D */
    RV_Display_RoundView_Left, 	        /* 左视图3D */
	RV_Display_RoundView_Right,	        /* 右视图3D */
    RV_Display_RoundView_FrontLeft,     /* 左前视图3D */
    RV_Display_RoundView_FrontRight, 	/* 右前视图3D */
    RV_Display_RoundView_BackLeft, 	    /* 左后视图3D */
	RV_Display_RoundView_BackRight,	    /* 右后视图3D */

    // 2D
    RV_Display_Origin_Front = 8,    /* 畸变原图-前 */
    RV_Display_Origin_Back,         /* 畸变原图-后 */
    RV_Display_Origin_Left,         /* 畸变原图-左 */
    RV_Display_Origin_Right,        /* 畸变原图-右 */
    RV_Display_Origin_All,          /* 畸变原图-全部*/
    RV_Display_Undistorted_Front,   /* 投影校正-前 */
    RV_Display_Undistorted_Back,    /* 投影校正-后 */
    RV_Display_SideView_Left,       /* 侧视视图-左 */
    RV_Display_SideView_Right,      /* 侧视视图-右 */
    RV_Display_SideView_Both,       /* 侧视视图-左右 */
    RV_Display_Manual_Calib_Front,  /* 手动校正-前 */
    RV_Display_Manual_Calib_Back,   /* 手动校正-后 */
    RV_Display_Manual_Calib_Left,   /* 手动校正-左 */
    RV_Display_Manual_Calib_Right,  /* 手动校正-右 */

    // 3D - higher views
    RV_Display_RoundView_High_Front = 22, /* 3D后视流媒体 */
    RV_Display_RoundView_High_Back, /* 显示门限提示 */
    RV_Display_RoundView_High_Left,
    RV_Display_RoundView_High_Right,
    RV_Display_RoundView_High_BackLeft,
    RV_Display_RoundView_High_BackRight,

    RV_Display_Origin_Select_Front,  /* 畸变原图-选择区域显示-前 */
    RV_Display_Origin_Select_Back,   /* 畸变原图-选择区域显示-后 */
    RV_Display_Origin_Select_Left,   /* 畸变原图-选择区域显示-左 */
    RV_Display_Origin_Select_Right,  /* 畸变原图-选择区域显示-右 */

    // 3D - turning linkage
    RV_Display_Front_Turn_Linkage,
    RV_Display_Back_Turn_Linkage,

    // 3D - round rotating
    RV_Display_RoundRotate = 34,

    // 2D - triple plane
    RV_Display_TriPlane_Front,  /* 三联屏-前 */
    RV_Display_TriPlane_Back,   /* 三联屏-后 */

    // 3D - turning views
    RV_Display_TurnView_Left_Front,   /* 左转向，往前看 */
    RV_Display_TurnView_Right_Front,  /* 右转向，往前看 */
    RV_Display_TurnView_Left_Back,    /* 左转向，往后看 */
    RV_Display_TurnView_Right_Back,   /* 右转向，往后看 */
    
    // 2D - tire views
    RV_Display_TireView_Front,
    RV_Display_TireView_Back,
    RV_Display_TireView_FrontLeft,
    RV_Display_TireView_FrontRight,
    RV_Display_TireView_BackLeft,
    RV_Display_TireView_BackRight,

    RV_Display_WidthLimit_Front,      /* 前视限宽视角，布局改变 */
    RV_Display_WidthLimit_Back,       /* 后视限宽视角，布局改变 */

    RV_Display_BirdView_Rotate,       /* 横向俯视视图 */

    RV_Display_Mode_Amount
} RVDisplayMode;

typedef enum _RVDisplayTemplate
{
    RV_Display_FullScreen = 0,          /* 全屏显示 */
    RV_Display_Birdview_Left,       /* 俯视视图靠左 */
    RV_Display_Birdview_Right,      /* 俯视视图靠右 */

    RV_Display_Template_Amount
} RVDisplayTemplate;

typedef enum _RVTraceType
{
    RV_Trace_None = 0,    // 不显示轨迹

    RV_Trace_Type_0,
    RV_Trace_Type_1,
    RV_Trace_Type_2,
    RV_Trace_Type_3,

    RV_Trace_Type_Amount
} RVTraceType;

typedef enum _RVCalibPatternType
{
    RV_Calib_Pattern_Auto_NoOverlap       = 1,
    RV_Calib_Pattern_Auto_ChessBoard      = 2,
    RV_Calib_Pattern_Auto_SmallBlock      = 3,
    RV_Calib_Pattern_Auto_BigBlock        = 4,
    RV_Calib_Pattern_Auto_ChessBoard_Test = 5,

    RV_Calib_Pattern_Manual_ChessBoard      = 9,
    RV_Calib_Pattern_Manual_SmallBlock      = 10,
    RV_Calib_Pattern_Manual_BigBlock        = 11,
    RV_Calib_Pattern_Manual_BigBlock_Only   = 12,
    RV_Calib_Pattern_Manual_ChessBoard_Test = 13,

    RV_Calib_Pattern_SmallBlock4 = 15,
    RV_Calib_Pattern_SmallBlock8 = 16
} RVCalibPatternType;

typedef enum _RVCarModelColor
{
    RV_Color_White = 0,
    RV_Color_Black,
    RV_Color_Blue,
    RV_Color_Red,
    RV_Color_Silver,
    RV_Color_Gray,
    RV_Color_Purple,

    RV_Color_Amount
} RVCarModelColor;

typedef enum _RVCarModelType
{
    RV_Car_Sedan = 0,
    RV_Car_SUV,
    RV_Car_Truck,   // 舍弃
    RV_Car_Business,

    RV_Car_Amount
} RVCarModelType;

typedef enum _RVCameraAngleMode
{
    RV_Angle_Mode_0 = 0,
    RV_Angle_Mode_1,
    RV_Angle_Mode_2,
    RV_Angle_Mode_3,

    RV_Angle_Mode_Amount
} RVCameraAngleMode;

typedef enum _RVLightState
{
    RV_Light_None = 0x0,
    RV_Light_Turning_Left = 0x1,  // 左右转向灯在同一时刻只有一个亮
    RV_Light_Turning_Right = 0x2,
    RV_Light_Stop = 0x4,          // 刹车灯
    RV_Light_Night = 0x8,         // 行车灯
    RV_Light_Warning = 0x10
} RVLightState;

typedef enum _Camera_Index_e
{
	Camera_Index_Front = 0x1,/*!< Front camera*/
	Camera_Index_Right = 0x2,/*!< Right camera*/
	Camera_Index_Rear = 0x4, /*!< Reverse camera*/
	Camera_Index_Left = 0x8, /*!< Left camera*/
	Camera_Index_All = 0xf,  /*!< Four cameras*/

    Camera_Index_Amount
} Camera_Index_e;

typedef enum _RVCalibStatus
{
    NotStart,   // 未开始
    Processing, // 处理中
    Success,    // 自动校正成功
    Failed,     // 自动校正失败

    Calib_Status_Amount
} RVCalibStatus;

typedef enum _RVTouchType
{
    None,       // 无操作
    Single,     // 单指（移动）操作
    Double,     // 双指（缩放）操作

    Touch_Type_Amount
} RVTouchType;

//扩展命令的结果
typedef enum _RVADASState
{
    RV_ADAS_MOD_Front_Left  = 0x1,      // 左前
    RV_ADAS_MOD_Front       = 0x2,	    // 前
    RV_ADAS_MOD_Front_Right = 0x4,	    // 右前
    RV_ADAS_MOD_Left        = 0x8,	    // 左
    RV_ADAS_MOD_Right       = 0x10,	    // 右
    RV_ADAS_MOD_Back_Left   = 0x20,	    // 左后
    RV_ADAS_MOD_Back        = 0x40,	    // 后
    RV_ADAS_MOD_Back_Right  = 0x80,	    // 右后

    RV_ADAS_BSD_Left        = 0x100,	// 左后
    RV_ADAS_BSD_Right       = 0x200,	// 右后

    RV_ADAS_DOW_Left        = 0x1000,   // 左后
    RV_ADAS_DOW_Right       = 0x2000,	// 右后

    //LDW 状态待定
    RV_ADAS_LDW_Left        = 0x10000,
    RV_ADAS_LDW_Right       = 0x20000,

    RV_ADAS_RESERVE         = 0x100000,
} RVADASState;

typedef enum _RVADASSwitch
{
    RV_ADAS_Use_MOD = 0x1,
    RV_ADAS_Use_BSD = 0x2,
    RV_ADAS_Use_DOW = 0x4,
    RV_ADAS_Use_LDW = 0x8
} RVADASSwitch;

typedef enum _RVScreenIndex
{
    RV_Screen_Small = 0x1,  // 偏小的视窗
    RV_Screen_Big   = 0x2   // 偏大的视窗（全屏也属于此项）
} RVScreenIndex;

typedef enum _RVDoorState
{
    RV_Door_Open_BackRight  = 0x1,
    RV_Door_Open_BackLeft   = 0x2,
    RV_Door_Open_FrontRight = 0x4,
    RV_Door_Open_FrontLeft  = 0x8
} RVDoorState;

typedef struct _RVTouchData
{
    RVTouchType touchType;  // 操作类型
    int16_t moveDis[2];     // 单指（移动）操作数据（xy值，与上一帧的差值，单位为像素）
    int16_t scaleDis;       // 双指（缩放）操作数据（两指间的距离，与上一帧的差值，单位为像素）
} RVTouchData;

typedef struct _RVCameraPoints
{
    int16_t numPoints;  // 4个镜头中的点数量
    int pointVertices[128]; // 镜头顺序，前后左右
} RVCameraPoints;

typedef struct _RVScreenRect
{
    RVScreenIndex screenIdx;
    uint16_t x;
    uint16_t y;
    uint16_t width;
    uint16_t height;
} RVScreenRect;

typedef struct _RVWheelAngleInfo
{
    int16_t maxSteeringAngle;   // 最大方向盘转角
    int16_t maxWheelAngle;      // 最大车轮转角
} RVWheelAngleInfo;

typedef struct _RVCameraPosition
{
    float disFront;          // 车头到前标定布的距离
    float disBack;           // 车尾到后标定布的距离
    float disMirrorToFront;  // 后视镜到车头的距离
    float heightCam[4];      // 4个镜头的高度，顺序：前后左右
} RVCameraPosition;

typedef struct _RVCarStructureInfo
{
    float wheelBase;          // 汽车轴距
    float treadFront;         // 前轮矩
    float disBackWheelToBack; // 后轮到车尾的距离
} RVCarStructureInfo;

typedef struct _RVRadarInfo
{
    float position[2];  // 雷达位置（与左标定布边缘的距离、与前标定布的距离），长度单位为米
    float angle;          // 雷达朝向的角度值，单位为度，左手坐标系：X轴正方向为车身右侧到左侧，Y轴正方向为车尾到车头;0度为X轴正方向，在俯视上，顺时针方向为度数增长方向
    float range;          // 雷达探测范围的角度值，单位为度
} RVRadarInfo;

typedef struct _RVRadarAllInfos
{
    uint8_t numRadars;
    RVRadarInfo info[20];    // 最多20个雷达数据
} RVRadarAllInfos;

/*! \brief Define roundview icar configuration
 *
 */
typedef struct _RoundViewConfig
{
    RVLensType lensType;
    RVSensorType sensorType;
    float carSize[3];
    uint32_t maxCarSpeed;
    RVWheelAngleInfo wheelInfo;
    float sideOffset;
    int cameraOffsetX[4];
    int cameraOffsetY[4];
    int cameraScale[4];     // abandon
    int birdViewScale;
    int showCarSpeed;
    int showWheelAngle;
    int showTurningLight;
    int showSteeringView;
    int showRadar;
    RVTraceType traceType;
    RVCarModelColor carModelColor;
    RVCarModelType carModelType;
    RVCalibPatternType calibPatternType;
    char saveFolderPath[128];
    RVCarStructureInfo carStructureInfo;
    RVCameraPosition camPositionInfo;
    RVCameraAngleMode camAngleMode;
    RVRadarAllInfos radarInfos;

    uint32_t camCenters[4][2];
    uint32_t markPoints[4][8][2];

    int version;
    int reserve[8];

    RVDisplayTemplate displayTemplate;
} RoundViewConfig, *pRoundViewConfig;

/*product type*/
typedef enum _ProductType_t
{
	Product_AHD,
	Product_NTSC,
	Product_PAL,
	Product_1080P
} ProductType_t;

/*data srouce type*/
typedef enum _DataType_t
{
	Data_Four,
	Data_One
} DataType_t;

typedef struct _RoundViewShareData
{
	int isNtsc;
	int displayMode;
	int cameraIdx;
	int svbvShow;

	//以下6个成员再创建结构体后, 需要配置好参数
	char	sysCfgPath[64];	    //系统配置路径
	int 	displayWidth;	    //显示区域宽度
	int 	displayHeight;	    //显示区域高度
	ProductType_t productType;	
	DataType_t dataType;
	
	G2dOpsS* g2DOpsS;
	RoundViewConfig config;
	RoundViewPrivate bvCfg;
} RoundViewShareData;

/* 3D环视算法设置EGL窗口 */
void RoundViewSetWindow(void *_window);

/* 3D环视算法默认的共享结构体参数 */
int RoundViewLoadDefaultParam(RoundViewShareData *param);

/* 3D环视算法初始化接口 */
int RoundViewInit(RoundViewShareData *param);

/* 3D环视算法渲染接口
 * RoundViewUpdate: 4个摄像头buf集成到一个buf的接口函数
 * RoundViewUpdateMulti: 4个摄像头buf分开的接口函数
 */
int RoundViewUpdate(struct V4L2BUF *buf);
int RoundViewUpdateMulti(struct V4L2BUF *fbuf,
                         struct V4L2BUF *bbuf,
                         struct V4L2BUF *lbuf,
                         struct V4L2BUF *rbuf);

/* 3D环视算法销毁接口 */
void RoundViewDestory (void);

/* 3D环视算法获取和设置接口 */
void RoundViewGetParam(RoundViewCMD command, void* params);
int RoundViewSetParam(RoundViewCMD command, void* params);

/* 3D环视算法授权激活接口 */
/*
 * param: activationCode 是输入的激活码
 *
 */
int RoundViewActivation(unsigned char *activationCode);

/* 3D环视算法调试接口, 参数为64个字符 */
int RoundViewDebug(RoundViewCMD command, void* params);

#ifdef __cplusplus
}
#endif

#endif //ROUNDVIEW_ROUNDVIEW_API_H
