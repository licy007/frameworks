/***********************************************************/
/*  RoundViewPrivate.h   									*/
/*                                                         */
/***********************************************************/

#ifndef _ROUNDVIEWPRIVATE_H_
#define _ROUNDVIEWPRIVATE_H_

#ifndef VD_STANDARD_TYPES_DECLARED
#define VD_STANDARD_TYPES_DECLARED
typedef unsigned long uint32;
typedef signed long int32;
typedef unsigned short uint16;
typedef signed short int16;
typedef unsigned char uint8;
typedef signed char int8;
#endif


#define NUM_VIEWS			4
#define NUM_PROJECT_ITEMS	9
#define NUM_REF_COL			6
#define NUM_REF_ROW			4
#define NUM_REF_POINTS		(NUM_REF_COL*NUM_REF_ROW)


/*
 * color types and stuff
 */
typedef enum {
	XRGB8888,
	RGB888,
	YUV422,
	YUV420,
	NUM_FORMAT_TYPE
} FormatTypes_t;

typedef enum _BVSensorType_e
{
	BVSensorType_0130 = 0,
	BVSensorType_1133,
	BVSensorType_225,
	BVSensorType_H65,
	BVSensorTypeAmount
}BVSensorType_e;

/*Lens type*/
typedef enum _BVLensType_e
{
	BVLensType_8255 = 0,
	BVLensType_4052,
	BVLensType_9005,
	BVLensType_2706,
	BVLensType_3011,
	BVLensType_8296,
	BVLensTypeAmount
}BVLensType_e;


typedef struct _Camera_t
{
	uint16		outH;
	uint16		outW;
	float		centerX;
	float		centerY;
	float		scaleH;
	float		scaleV;
	uint16		mirror;
	uint16		reserve;

	BVSensorType_e sensorType;
	BVLensType_e	lensType;
}Camera_t, *pCamera_t;

/*Bridview configurate parameters*/
typedef struct _BV_Config_t 
{
	uint32		size;
	uint32		crc;
	int32		mDistance;
	Camera_t	mCameras[NUM_VIEWS];
	int16		refExisted;
	int16		matrixExisted;
	float		projectH[NUM_VIEWS][NUM_PROJECT_ITEMS];
	float		refPoints[NUM_VIEWS][NUM_REF_POINTS * 2];
	int32		patternPos[NUM_VIEWS][NUM_REF_POINTS * 2];
	int32		carW;
	int32		carH;
	int32		sideOffset;
	int32		carOffsetX;
	int32		carOffsetY;
	int32		mFrontOffset;
	int32		mRearOffset;
	int32		inpW;
	int32		inpH;
	int32		inPitch;
	int32		outPitch;
	uint32 		combinedViewWidth;
	uint32 		combinedViewHeight;
	uint32 		BirdViewWidth;
	uint32 		BirdViewHeight;
	uint32 		SideViewWidth;
	uint32 		SideViewHeight;
	uint32 		SideViewDisplayedFOV;
	uint32 		SideViewUL_X;
	uint32 		SideViewUL_Y;
	uint32 		BirdViewUL_X;
	uint32 		BirdViewUL_Y;
	uint32 		externalSideview;
	FormatTypes_t format;
	uint32	patternType;
	float	projectSideH[NUM_VIEWS][NUM_PROJECT_ITEMS];
}BV_Config_t, *pBV_Config_t;


typedef struct _BV_Config_t RoundViewPrivate;
#endif 
