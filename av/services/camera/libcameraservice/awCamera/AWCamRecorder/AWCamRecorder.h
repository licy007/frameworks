#ifndef AWCAMRECORDER_H_
#define AWCAMRECORDER_H_

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <getopt.h>
#include <sys/wait.h>
#include <utils/Thread.h>
#include <pthread.h>
#include <sys/types.h>
#include <ctype.h>

#include <utils/Log.h>

#include <binder/IPCThreadState.h>
#include <utils/Errors.h>
#include <utils/Thread.h>
#include <utils/Timers.h>
#include <media/stagefright/AudioSource.h>
#include <media/stagefright/MediaMuxer.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/MetaData.h>
#include <media/stagefright/MediaCodec.h>
#include <media/stagefright/Utils.h>
#include <media/stagefright/MediaDefs.h>
#include <media/ICrypto.h>
#include <gui/Surface.h>
#include "libOSAL/OSAL_Queue.h"
#include "libAWEnc/AWEnc.h"
#include "../AWCameraCommon.h"

enum AWCamRecFileFormat {
    AW_CAM_REC_FILE_H264_TS = 0,
    AW_CAM_REC_FILE_H265_TS = 1,
    AW_CAM_REC_FILE_H264_MP4 = 2,
    AW_CAM_REC_FILE_H265_MP4 = 3
};

enum AWCamRecAudioMode {
    AW_CAM_REC_AUIOD_MODE_OFF = 0,
    AW_CAM_REC_AUIOD_MODE_NORMAL = 1
};

namespace android {
using android::status_t;

class AWEnc;

class AWCamRecorder{

public:
    /* Constructs AWCamRecorder instance. */
    AWCamRecorder(int cameraId,int width,int height);

    /* Destructs AWCamRecorder instance. */
    ~AWCamRecorder();

public:

    bool getInputVideoDataThread();
    bool getEncDataToMuxerThread();
    bool getInputAudioDataThread();
    bool getAudioEncToMuxerThread();

protected:

    class DoInputVideoDataThread : public Thread {
        AWCamRecorder*   mAWCamRecorder;
        bool        mRequestExit;
    public:
        DoInputVideoDataThread(AWCamRecorder* dev) :
            Thread(false),
            mAWCamRecorder(dev),
            mRequestExit(false) {
        }
        void startThread() {
            run("InputVideoDataThread", PRIORITY_URGENT_DISPLAY);
        }
        void stopThread() {
            mRequestExit = true;
        }
        virtual bool threadLoop() {
            if (mRequestExit) {
                return false;
            }
            return mAWCamRecorder->getInputVideoDataThread();
        }
    };

    class DoInputAudioDataThread : public Thread {
        AWCamRecorder*   mAWCamRecorder;
        bool        mRequestExit;
    public:
        DoInputAudioDataThread(AWCamRecorder* dev) :
            Thread(false),
            mAWCamRecorder(dev),
            mRequestExit(false) {
        }
        void startThread() {
            run("InputAudioDataThread", PRIORITY_AUDIO);
        }
        void stopThread() {
            mRequestExit = true;
        }
        virtual bool threadLoop() {
            if (mRequestExit) {
                return false;
            }
            return mAWCamRecorder->getInputAudioDataThread();
        }
    };

    class DoGetEncDataToMuxerThread : public Thread {
        AWCamRecorder*   mAWCamRecorder;
        bool        mRequestExit;
    public:
        DoGetEncDataToMuxerThread(AWCamRecorder* dev) :
            Thread(false),
            mAWCamRecorder(dev),
            mRequestExit(false) {
        }
        void startThread() {
            run("DoGetEncDataToMuxerThread", PRIORITY_URGENT_DISPLAY);
        }
        void stopThread() {
            mRequestExit = true;
        }
        virtual bool threadLoop() {
            if (mRequestExit) {
                return false;
            }
            return mAWCamRecorder->getEncDataToMuxerThread();
        }
    };


    class DoGetAudioEncToMuxerThread : public Thread {
        AWCamRecorder*   mAWCamRecorder;
        bool        mRequestExit;
    public:
        DoGetAudioEncToMuxerThread(AWCamRecorder* dev) :
            Thread(false),
            mAWCamRecorder(dev),
            mRequestExit(false) {
        }
        void startThread() {
            run("DoGetAudioEncToMuxerThread", PRIORITY_URGENT_DISPLAY);
        }
        void stopThread() {
            mRequestExit = true;
        }
        virtual bool threadLoop() {
            if (mRequestExit) {
                return false;
            }
            return mAWCamRecorder->getAudioEncToMuxerThread();
        }
};


public:
    status_t awCamRecInit(char * filename, int format,
                          int width, int height, int framerate, int bitrate,
                          int audiomode, int audiobitrate);
    status_t awCamRecStart();
    status_t awCamRecStop();
    status_t awCamRecSetParaAndCmds(char * parameters, int cmds);
    char*    awCamRecGetParaAndCmds(int cmds);
    status_t awCamRecStartNextFile(char * filename);
    status_t awCamRecRelease();
    status_t awCamRecOnError(int errorcode);
    bool     awCamRecIsStart(void);
    void     awCameraRecSetReleaseFrameCbk(awCamReleaseFrame awCamRecReleasecb, void* owner);
    bool awCamRecInputBuffer(V4L2BUF_t *cap_buffer);

private:

    status_t stopVideoInputThread();
    status_t stopEncToMuxerThread();
    status_t stopAudioInputThread();
    status_t stopAudioEncToMuxerThread();
    status_t awCamRecSetH264BasePara(int width, int height, int framerate, int bitrate);
    status_t awCamRecSetH265BasePara(int width, int height, int framerate, int bitrate);
    status_t initMuxerVideoFormat();
    status_t getSPSPPS();
    status_t initMuxerAudioFormat();
    status_t prepareAudioEncoder();
    void     releaseInputVideoBuffer();

public:

    int                             mCameraId;
    OSAL_QUEUE                      mCaptureQueueBuffer;
    awCamReleaseFrame            mAWCamRecReleaseFrame;
    void*                           mCbkOwner;
    int                             mInputWidth;
    int                             mInputHeight;

private:

    AWEnc                           *mAWEnc;
    Mutex                           mAWCamRecLock;
    Mutex                           mAWCamRecNextFileLock;

    sp<DoInputVideoDataThread>      mInputVideoDataThread;
    pthread_mutex_t                 mInputVideoDataMutex;
    pthread_cond_t                  mInputVideoDataOneLoopFinishCond;
    pthread_cond_t                  mInputVideoDataCond;

    sp<DoGetEncDataToMuxerThread>   mGetEncDataToMuxerThread;
    pthread_mutex_t                 mGetEncDataToMuxerMutex;
    pthread_mutex_t                 mGetEncMuxerSwitchMutex;
    pthread_cond_t                  mGetEncDataToMuxerOneLoopFinishCond;
    pthread_cond_t                  mGetEncDataToMuxerKeyFrameCond;
    pthread_cond_t                  mGetEncOutputDataCond;

    pthread_mutex_t                 mMuxerMutex;
    pthread_cond_t                  mMuxerCondWaitSync;

    sp<AudioSource>                 mAudioSource;
    String16                        *mOpPackageName;

    sp<AMessage>                    mVideoFormat;
    sp<ABuffer>                     mCsd0;
    sp<ABuffer>                     mCsd1;


    //status flag
    int                             mInputVideoDataThreadStatu;
    int                             mGetEncDataToMuxerThreadStatu;

    int                             mMuxerActiveStatu;

    VencH264Param                   mH264Param;
    VencH265Param                   mH265Param;
    VencBaseConfig                  mBaseConfig;
    VencInputBuffer                 mInputBuffer;
    VencOutputBuffer                mOutputBuffer;
    VencHeaderData                  mSPSPPSData;
    VencHeaderData                  mSPSData;
    VencHeaderData                  mPPSData;


    sp<MediaMuxer>                  mMuxer0;
    ssize_t                         mVideoTrackIdx0;
    ssize_t                         mAudioTrackIdx0;
    sp<MediaMuxer>                  mMuxer1;
    ssize_t                         mVideoTrackIdx1;
    ssize_t                         mAudioTrackIdx1;
    sp<MetaData>                    mAudioTrackMeta;

    MediaMuxer::OutputFormat        mMuxerFormat;

    unsigned char*                  mSwitchpBuffer;
    unsigned int                    mSwitchBufferSize;

    VENC_CODEC_TYPE                 mCodecType;

    int                             mAudioMode;
    int                             mInputAudioDataThreadStatu;
    sp<DoInputAudioDataThread>      mInputAudioDataThread;
    pthread_mutex_t                 mInputAudioDataMutex;
    pthread_cond_t                  mInputAudioDataOneLoopFinishCond;
    pthread_cond_t                  mInputAudioDataCond;

    sp<AMessage>                    mAudioFormat;
    int                             mAudioBitrate;
    sp<MediaCodec>                  mAudioEncoder;
    int                             mDequeueBufferTimeout;
    int                             mAudioMuxerThreadStatu;

    sp<DoGetAudioEncToMuxerThread>  mGetAudioEncToMuxerThread;
    pthread_mutex_t                 mAudioMuxerMutex;
    pthread_cond_t                  mAudioMuxerCondOneLoopFinish;
    int                             mMuxer0Statu;
    bool                            mNeedAudioRecord;
    int                             mBufCnt;
    bool mIsRunning;
    int64_t mVideoBaseTime;
};
};
#endif
