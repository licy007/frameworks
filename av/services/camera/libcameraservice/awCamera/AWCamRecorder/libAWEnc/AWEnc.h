#ifndef __AWENC_H__
#define __AWENC_H__

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <utils/Thread.h>
#include <utils/Log.h>

#include <pthread.h>
#include "vencoder.h"

#define DBG_ENABLE 0

namespace android {

class AWEnc {
public:
    /* Constructs AWEnc instance. */
    AWEnc();

    /* Destructs AWEnc instance. */
    ~AWEnc();

public:

    status_t encInit(VENC_CODEC_TYPE eCodecType, void *h264orH265Param, VencBaseConfig *baseConfig, VencHeaderData *SPSPPSData);
    status_t encUnInit();
    status_t addOneInputFrame(VencInputBuffer* pInputbuffer);
    status_t encOneFrame(VencInputBuffer* pInputbuffer);
    status_t getOneBitStreamFrame(VencOutputBuffer *pOutputBuffer);
    status_t freeOneBitStreamFrame(VencOutputBuffer *pOutputBuffer);

    status_t getSPSPPS();
    status_t setSize();
    status_t setFramerate(int framerate);
    status_t setBitrate(int bitrate);
    status_t requestIFrame();

    status_t saveFrame(char *str, void *p, int length, int is_oneframe);

private:

public:

    VideoEncoder*                   pVideoEnc;
    int                             mAWEncID;
};
}
#endif  /* __AWENC_H__ */

