//AWCameraCommon.h
//add by fengyun 20190923
#ifndef __AW_CAMERA_COMMON__
#define __AW_CAMERA_COMMON__
#include <log/log.h>

#define F_LOG ALOGD("AWCamera %s, line: %d", __FUNCTION__, __LINE__);
#define LOGE ALOGE
#define LOGD ALOGD
#define LOGV ALOGV
//namespace android {

/*  Four-character-code (FOURCC) */
#define v4l2_fourcc(a, b, c, d)\
	((__u32)(a) | ((__u32)(b) << 8) | ((__u32)(c) << 16) | ((__u32)(d) << 24))
#define V4L2_PIX_FMT_NV12    v4l2_fourcc('N', 'V', '1', '2') /* 12  Y/CbCr 4:2:0  */
#define V4L2_PIX_FMT_NV21    v4l2_fourcc('N', 'V', '2', '1') /* 12  Y/CrCb 4:2:0  */
#define ALIGN_4K(x) (((x) + (4095)) & ~(4095))
#define ALIGN_32B(x) (((x) + (31)) & ~(31))
#define ALIGN_16B(x) (((x) + (15)) & ~(15))
#define ALIGN_8B(x) (((x) + (7)) & ~(7))

#define GPU_BUFFER_ALIGN ALIGN_16B
#define KEY_SNAP_PATH   "picture-path"

typedef void (*awCamReleaseFrame)(void* owner, int index,int sharefd);
typedef void (*awCamReleaseCallback)(void*user, int index);
typedef void (*notify_callback)(int32_t msgType,
                            int32_t ext1,
                            int32_t ext2,
                            void* user);

typedef struct PREVIEWINFO_t
{
    int left;
    int top;
    int width;            // preview width
    int height;            // preview height
}PREVIEWINFO_t, RECT_t;

typedef struct V4L2BUF_t
{
    unsigned long            addrPhyY;        // physical Y address of this frame
    unsigned long            addrPhyC;        // physical Y address of this frame
    unsigned long            addrVirY;        // virtual Y address of this frame
    unsigned long            addrVirC;        // virtual Y address of this frame
    unsigned int    width;
    unsigned int    height;
    int             index;            // DQUE id number
    long long        timeStamp;        // time stamp of this frame
    RECT_t            crop_rect;
    int                format;
    void*           overlay_info;

    // thumb
    unsigned char    isThumbAvailable;
    unsigned char    thumbUsedForPreview;
    unsigned char    thumbUsedForPhoto;
    unsigned char    thumbUsedForVideo;
    unsigned long            thumbAddrPhyY;        // physical Y address of thumb buffer
    unsigned long            thumbAddrVirY;        // virtual Y address of thumb buffer
    unsigned int    thumbWidth;
    unsigned int    thumbHeight;
    RECT_t            thumb_crop_rect;
    int             thumbFormat;
    int             refCnt;         // used for releasing this frame
    unsigned int    bytesused;      // used by compressed source
    int             nDmaBufFd;      //dma fd callback to codec
    int             nShareBufFd;    //share fd callback to codec
}V4L2BUF_t;
//};  // namespace android

#endif //__AW_CAMERA_COMMON__
