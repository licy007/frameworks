//AWCameraPreview.h
//add by fengyun 20190923

#ifndef __AW_CAMERA_PREVIEW__
#define __AW_CAMERA_PREVIEW__
#include <system/window.h>
#include <binder/IMemory.h>
#include <binder/MemoryBase.h>
#include <binder/MemoryHeapBase.h>
#include <utils/RefBase.h>
#include "AWCameraCommon.h"
namespace android{
#define BUF_PREVIEW_CNT 3

class AWCameraPreview :public virtual RefBase{

public:
    AWCameraPreview(const char *name,int cameraId,int cameraNum,int width,int height);
    virtual ~AWCameraPreview();
    int setPreviewWindow(const sp<ANativeWindow>& window);
    int stopPreview();
    int startPreview();
    int onCameraPreivewWithHandle(int sharefd,int width,int height);
    int onCameraPreivewWithHandle(void  * g2dOpss,V4L2BUF_t *v4l2buffer);
    void setPreviewFormat(int format);
private:
    String8 mName;
    int mCameraId;
    int mCameraNum;
    sp<ANativeWindow>  mPreviewWindow;
    int mEnablePreview;
    Mutex mObjectLock;
    int mPreviewFrameWidth;
    int mPreviewFrameHeight;
    int mPreviewSize;
    int mPreviewFormat;
    bool mNeedInitWind;
};
}
#endif //__AW_CAMERA_PREVIEW__
