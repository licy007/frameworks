
/*
* Copyright (c) 2008-2016 Allwinner Technology Co. Ltd.
* All rights reserved.
*
* File : memoryAdapter.h
* Description :
* History :
*   Author  : xyliu <xyliu@allwinnertech.com>
*   Date    : 2016/04/13
*   Comment :
*
*
*/

#ifndef AW_MEMORY_ADAPTER_H
#define AW_MEMORY_ADAPTER_H

#include "aw_sc_interface.h"

#ifdef __cplusplus
extern "C" {
#endif

//* get current ddr frequency, if it is too slow, we will cut some spec off.
//int MemAdapterGetDramFreq();
struct AWScCamMemOpsS* MemCamAdapterGetOpsS();

#ifdef __cplusplus
}
#endif

#endif /* AW_MEMORY_ADAPTER_H */
