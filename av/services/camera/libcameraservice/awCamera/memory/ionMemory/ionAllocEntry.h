
/*
* Copyright (c) 2008-2016 Allwinner Technology Co. Ltd.
* All rights reserved.
*
* File : ionAllocEntry.h
* Description :
* History :
*   Author  : xyliu <xyliu@allwinnertech.com>
*   Date    : 2016/04/13
*   Comment :
*
*
*/
#ifndef AW_ION_ENTRY_H
#define AW_ION_ENTRY_H

struct AWScCamMemOpsS* __GetIonCamMemOpsS();
#endif

