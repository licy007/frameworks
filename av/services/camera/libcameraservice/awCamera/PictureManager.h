
#ifndef __PICTURE_MANAGER_H__
#define __PICTURE_MANAGER_H__
#include <utils/Mutex.h>
#include <utils/Errors.h>
#include <utils/Thread.h>
#include "vencoder.h"
#include "AWCamRecorder/libOSAL/OSAL_Queue.h"
#include "AWCameraCommon.h"
namespace android {
enum ThreadState {
    THREAD_STATE_NULL,        // The thread has not been created.
    THREAD_STATE_PAUSED,    // The thread is paused for waiting some signal.
    THREAD_STATE_RUNNING,    // The thread is in running.
    THREAD_STATE_EXIT,        // The thread will exit.
};
class PictureManager {
public:
    /* Constructs PictureManager instance. */
    PictureManager(int cameraId,int width,int height);

    /* Destructs PictureManager instance. */
    ~PictureManager();

public:
    /* Actual handler for camera_device_ops_t::set_callbacks callback.
     * This method is called by the containing V4L2Camera object when it is
     * handing the camera_device_ops_t::set_callbacks callback.
     */
    void setCallbacks(notify_callback notify_cb,awCamReleaseFrame relase,void* user);
    /* Sets JPEG quality used to compress frame during picture taking. */
    void setJpegQuality(int jpeg_quality)
    {
        mJpegQuality = jpeg_quality;
    }

protected:
    /* Locks this instance for data change. */
    Mutex                           mObjectLock;

    /*
     * Callbacks, registered in set_callbacks.
     */

    notify_callback          mNotifyCB;
    void*                           mCallbackCookie;
    uint32_t                        mMessageEnabler;
public:
    inline void setPictureSize(int w, int h)
    {
        mPictureWidth = ALIGN_16B(w);
        mPictureHeight = ALIGN_16B(h);
    }

    inline void setExifMake(char * make)
    {
        strcpy(mExifMake, make);
    }

    inline void setExifModel(char * model)
    {
        strcpy(mExifModel, model);
    }

    // Sets JPEG rotate used to compress frame during picture taking.
    inline void setJpegRotate(int jpeg_rotate)
    {
        mJpegRotate = jpeg_rotate;
    }

    inline void setGPSLatitude(double gpsLatitude)
    {
        mGpsLatitude = gpsLatitude;
    }

    inline void setGPSLongitude(double gpsLongitude)
    {
        mGpsLongitude = gpsLongitude;
    }

    inline void setGPSAltitude(double gpsAltitude)
    {
        mGpsAltitude = gpsAltitude;
    }

    inline void setGPSTimestamp(long gpsTimestamp)
    {
        mGpsTimestamp = gpsTimestamp;
    }

    inline void setGPSMethod(const char * gpsMethod)
    {
        strcpy(mGpsMethod, gpsMethod);
    }

    inline void setJpegThumbnailSize(int w, int h)
    {
        mThumbWidth = w;
        mThumbHeight = h;
    }
    inline void setFocalLenght(double val)
    {
        mFocalLength = val;
    }

    inline void setWhiteBalance(int whitebalance)
    {
        mWhiteBalance = whitebalance;
    }

    inline void setCallingProcess(char * str)
    {
        strcpy(mCallingProcessName, str);
    }

    inline void setSaveFolderPath(const char * str)
    {
        strcpy(mFolderPath, str);
    }
    inline void setSnapPath(const char * str)
    {
        strncpy(mSnapPath, str,sizeof(mSnapPath) -1);
    }

    inline void setCBSize(int w, int h)
    {
        mCBWidth = w;
        mCBHeight = h;
    }

    void setPictureMode(bool single)
    {
        mIsSinglePicture = single;
    }
    inline int isMessageEnabled(uint msg_type)
    {
        return mMessageEnabler & msg_type;
    }

    inline bool isPictureEnable()
    {
        return mEnablePicture;
    }

    bool queueCaptureBuf(void* frame, bool is_continuous = false);
    int takePicture();
    int encodePicture( void* frame);
    void notifyPictureMsg(const void* frame);
    void getCurrentDateTime();
    void open();
    void cleanup();
#if 0
    void setExifInfo(struct isp_exif_attribute exifinfo,int zoom_ratio,int exposure_bias);
#endif
protected:

    class DoSavePictureThread : public Thread {
        PictureManager* mPictureManager;
        ThreadState mThreadStatus;
    public:
        DoSavePictureThread(PictureManager* cb) :
            Thread(false),
            mPictureManager(cb),
            mThreadStatus(THREAD_STATE_NULL) {
        }
        void startThread() {
            mThreadStatus = THREAD_STATE_RUNNING;
            run("DoSavePictureThread", PRIORITY_DISPLAY);
        }
        void stopThread() {
            mThreadStatus = THREAD_STATE_EXIT;
        }
        ThreadState getThreadStatus() {
            return mThreadStatus;
        }
        bool isThreadStarted() {
            return (mThreadStatus == THREAD_STATE_PAUSED) || (mThreadStatus == THREAD_STATE_RUNNING);
        }
        virtual bool threadLoop() {
            return mPictureManager->savePictureThread();
        }
    };

    bool                             savePictureThread();

    sp<DoSavePictureThread>         mSavePictureThread;
    pthread_mutex_t                 mSavePictureMutex;
    pthread_cond_t                    mSavePictureCond;
protected:

    // calling process name for some app, such as facelock
    char                            mCallingProcessName[128];

    // JPEG quality used to compress frame during picture taking.
    int                             mJpegQuality;

    // JPEG rotate used to compress frame during picture taking.
    int                                mJpegRotate;

    // JPEG size
    int                                mPictureWidth;
    int                                mPictureHeight;

    // thumb size
    int                                mThumbWidth;
    int                                mThumbHeight;
    // gps exif
    double                          mGpsLatitude;
    double                            mGpsLongitude;
    double                            mGpsAltitude;
    long                            mGpsTimestamp;
    char                            mGpsMethod[100];
    double                            mFocalLength;
    int                             mWhiteBalance;

    char                              mExifMake[64];        //for the cameraMake name
    char                              mExifModel[64];        //for the cameraMode
    char                              mDateTime[64];        //for the data and time
    char                            mFolderPath[128];
    char                            mSnapPath[128];
    int                             mFd;
    rational_t        mExposureTime;
    rational_t        mFNumber;
    short            mISOSpeed;
    rational_t         mExposureBiasValue;
    short            mMeteringMode;
    short            mFlashUsed;
    rational_t        mFocalLength_r;
    rational_t        mDigitalZoomRatio;
    short            mExposureMode;
    // cb size
    int                                mCBWidth;
    int                                mCBHeight;


    bool                            mSaveThreadExited;
    bool                            mIsSinglePicture;
    OSAL_QUEUE   mQueueBufferPicture;
    awCamReleaseFrame mReleaseCameraFrame;
    bool mEnablePicture;
    int mCameraId;
#if TAKE_PIC_USE_COPY
    struct ScCamMemOpsS *mMemOpss;
    V4L2BUF_t mPicBuffer;
#endif
};

}; /* namespace android */

#endif  /* __PICTURE_MANAGER_H__ */
