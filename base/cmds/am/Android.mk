# Copyright 2008 The Android Open Source Project
#
LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := \
    $(call all-java-files-under, src) \
    $(call all-proto-files-under, proto)
LOCAL_MODULE := am
LOCAL_PROTOC_OPTIMIZE_TYPE := stream
LOCAL_POST_INSTALL_CMD := mkdir  -p $(TARGET_OUT_VENDOR)/framework; \
    cp $(TARGET_OUT)/framework/am.jar $(TARGET_OUT_VENDOR)/framework/vendor_am.jar
include $(BUILD_JAVA_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := am
LOCAL_SRC_FILES := am
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_TAGS := optional
LOCAL_POST_INSTALL_CMD := \
    mkdir -p $(TARGET_OUT_VENDOR)/xbin; \
    cp $(TARGET_OUT)/bin/am $(TARGET_OUT_VENDOR)/xbin/am
include $(BUILD_PREBUILT)
