/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server.pm;

import com.android.internal.util.XmlUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.content.ComponentName;
import android.content.IntentFilter;
import android.util.Log;

import java.io.IOException;

class PreferredActivity extends IntentFilter implements PreferredComponent.Callbacks {
    private static final String TAG = "PreferredActivity";

    private static final boolean DEBUG_FILTERS = false;

    final PreferredComponent mPref;

    PreferredActivity(IntentFilter filter, int match, ComponentName[] set, ComponentName activity,
            boolean always) {
        super(filter);
        mPref = new PreferredComponent(this, match, set, activity, always);
    }

    PreferredActivity(XmlPullParser parser) throws XmlPullParserException, IOException {
        mPref = new PreferredComponent(this, parser);
    }

    public void writeToXml(XmlSerializer serializer, boolean full) throws IOException {
        mPref.writeToXml(serializer, full);
        serializer.startTag(null, "filter");
            super.writeToXml(serializer);
        serializer.endTag(null, "filter");
    }

    public boolean onReadTag(String tagName, XmlPullParser parser) throws XmlPullParserException,
            IOException {
        if (tagName.equals("filter")) {
            if (DEBUG_FILTERS) {
                Log.i(TAG, "Starting to parse filter...");
            }
            readFromXml(parser);
            if (DEBUG_FILTERS) {
                Log.i(TAG, "Finished filter: depth=" + parser.getDepth() + " tag="
                        + parser.getName());
            }
        } else {
            PackageManagerService.reportSettingsProblem(Log.WARN,
                    "Unknown element under <preferred-activities>: " + parser.getName());
            XmlUtils.skipCurrentTag(parser);
        }
        return true;
    }

    private boolean FilterEquals(IntentFilter pa) {
        boolean auto_verify = pa.getAutoVerify();
        int action_count = pa.countActions();
        int type_count = pa.countDataTypes();
        int category_count = pa.countCategories();
        int schemes_count = pa.countDataSchemes();
        int schemeSpecial_count = pa.countDataSchemeSpecificParts();
        int authorities_count = pa.countDataAuthorities();
        int dataPath_count = pa.countDataPaths();
        int i = 0;

        if (auto_verify != this.getAutoVerify())
            return false;
        if (action_count != this.countActions()) {
            return false;
        } else {
            for (i = 0; i < action_count; i++) {
                if (!this.hasAction(pa.getAction(i)))
                    return false;
            }
        }
        if (type_count != this.countDataTypes()) {
            return false;
        } else {
            for (i = 0; i < type_count; i++) {
                if (!this.hasDataType(pa.getDataType(i)))
                    return false;
            }
        }
        if (category_count != this.countCategories()) {
            return false;
        } else {
            for (i = 0; i < category_count; i++) {
                if (!this.hasCategory(pa.getCategory(i)))
                    return false;
            }
        }
        if (schemes_count != this.countDataSchemes()) {
            return false;
        } else {
            for (i = 0; i < schemes_count; i++) {
                if (!this.hasDataScheme(pa.getDataScheme(i)))
                    return false;
            }
        }
        if (schemeSpecial_count != this.countDataSchemeSpecificParts()) {
            return false;
        } else {
            for (i = 0; i < schemeSpecial_count; i++) {
                if (!this.hasDataSchemeSpecificPart(pa.getDataSchemeSpecificPart(i)))
                    return false;
            }
        }
        if (authorities_count != this.countDataAuthorities()) {
            return false;
        } else {
            for (i = 0; i < authorities_count; i++) {
                if (!this.hasDataAuthority(pa.getDataAuthority(i)))
                    return false;
            }
        }
        if (dataPath_count != this.countDataPaths()) {
            return false;
        } else {
            for (i = 0; i < dataPath_count; i++) {
                if (!this.hasDataPath(pa.getDataPath(i)))
                    return false;
            }
        }
        return true;
    }

    public boolean equals(PreferredActivity pa) {
        return FilterEquals(pa) && this.mPref.equals(pa.mPref);
    }

    @Override
    public String toString() {
        return "PreferredActivity{0x" + Integer.toHexString(System.identityHashCode(this))
                + " " + mPref.mComponent.flattenToShortString() + "}";
    }
}
