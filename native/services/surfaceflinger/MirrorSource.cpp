
#include "DisplayDevice.h"
#include "Layer.h"
#include "MirrorSource.h"

namespace android {

MirrorSource::MirrorSource(const sp<DisplayDevice>& displayDevice, sp<Layer>& layer)
    : mSourceDisplay(displayDevice), mSourceLayer(layer)
{ }

MirrorSource::~MirrorSource() = default;

sp<Layer> MirrorSource::sourceLayer()
{
    return mSourceLayer;
}

sp<DisplayDevice> MirrorSource::sourceDisplay()
{
    return mSourceDisplay;
}

std::unique_ptr<MirrorSource> createMirrorSource(
        const sp<DisplayDevice>& displaydevice, sp<Layer>& layer) {
    auto result = std::make_unique<MirrorSource>(displaydevice, layer);
    return result;
}

} // namespace android
