
#ifndef MIRROR_SOURCE_H
#define MIRROR_SOURCE_H

#include <memory>
#include <utils/RefBase.h>

namespace android {

class DisplayDevice;
class Layer;

class MirrorSource {
public:
    MirrorSource(const sp<DisplayDevice>& displayDevice, sp<Layer>& layer);
   ~MirrorSource();

    sp<Layer> sourceLayer();
    sp<DisplayDevice> sourceDisplay();

private:
    sp<DisplayDevice> mSourceDisplay;
    sp<Layer> mSourceLayer;
};

std::unique_ptr<MirrorSource> createMirrorSource(
        const sp<DisplayDevice>& displayDevice,
        sp<Layer>& layer);

} // namespace android

#endif
